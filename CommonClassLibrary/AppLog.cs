﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CommonClassLibrary {

    // универсальный класс для ведения лога приложения
    // формирует два лог файла: один общий на все запуски программы, второй - на текущую сессию. main log и short log
    // класс годится только для десктопных приложений. для организаций логи должны писаться в БД
    static public class AppLog {
        private static Boolean initialized;
        private static String logFilename;
        private static String shortLogFilename;
        private static readonly Int32 mainThreadId;

        static private Dictionary<Int32, String> falseCauses; // key is thread id

        [DllImport("Kernel32", EntryPoint = "GetCurrentThreadId", ExactSpelling = true)]
        private static extern Int32 GetCurrentWin32ThreadId();

        static AppLog() {
            mainThreadId = GetCurrentWin32ThreadId();
        }
        static public void Init(String folder, String filenamePrefix) {
            try {
                falseCauses = new Dictionary<Int32, String>();

                logFilename = Path.Combine(folder, filenamePrefix + "-main.log");
                shortLogFilename = Path.Combine(folder, filenamePrefix + "-short.log");

                if (File.Exists(shortLogFilename)) {
                    File.Delete(shortLogFilename);
                }
                initialized = true;
            }
            catch {
                //
            }
        }

        static private void LogError(String error, MethodBase methodBase) {
            if (!initialized) return;

            LogIt("Error " + error, methodBase);
        }

        static private void LogTrace(String trace, MethodBase methodBase) {
            if (!initialized) return;

            LogIt(trace, methodBase);
        }

        static public void LogTrace(String trace) {
            if (!initialized) return;

            StackTrace stackTrace = new StackTrace();
            MethodBase methodBase = stackTrace.GetFrame(1).GetMethod();

            LogIt(trace, methodBase);
        }

        static public void LogTrace(String falseCause, params object[] args) {
            if (!initialized) return;

            try {
                string sFormatted = String.Format(falseCause, args);
                falseCauses[GetCurrentWin32ThreadId()] = sFormatted;

                StackTrace stackTrace = new StackTrace();
                MethodBase methodBase = stackTrace.GetFrame(1).GetMethod();

                AppLog.LogTrace(sFormatted, methodBase);
            }
            catch {
                //
            }
        }

        static private void LogIt(String text, MethodBase method) {
            try {
                FileStream fs = new FileStream(logFilename, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
                String threadId = mainThreadId == GetCurrentWin32ThreadId() ? String.Empty : GetCurrentWin32ThreadId().ToString();
                String line = String.Format("{0}\t{1}\t{2}\t{3}\t{4}", DateTime.Now, threadId, text, method.DeclaringType.ToString(), method.ToString());
                sw.WriteLine(line);
                sw.Dispose();
                fs.Dispose();

                fs = new FileStream(shortLogFilename, FileMode.Append);
                sw = new StreamWriter(fs, Encoding.UTF8);
                sw.WriteLine(line);
                sw.Dispose();
                fs.Dispose();
            }
            catch {
                // nope;
            }
        }

        // ********************************

        static public void AppendError(String falseCause, params object[] args) {
            try {
                string sFormatted = String.Format(falseCause, args);

                Int32 currentThreadId = GetCurrentWin32ThreadId();
                if (falseCauses.ContainsKey(currentThreadId))
                    falseCauses[currentThreadId] = sFormatted + " " + falseCauses[currentThreadId];
                else
                    falseCauses[currentThreadId] = sFormatted;

                StackTrace stackTrace = new StackTrace();
                MethodBase methodBase = stackTrace.GetFrame(1).GetMethod();

                AppLog.LogError(sFormatted, methodBase);
            }
            catch {
                //
            }
        }

        static public void Log(Exception x) {
            falseCauses[GetCurrentWin32ThreadId()] = x.Message;

            StackTrace stackTrace = new StackTrace();
            MethodBase methodBase = stackTrace.GetFrame(1).GetMethod();

            AppLog.LogError(x.Message, methodBase);
        }

        static public void LogError(String falseCause, params object[] args) {
            try {
                string sFormatted = String.Format(falseCause, args);
                falseCauses[GetCurrentWin32ThreadId()] = sFormatted;

                StackTrace stackTrace = new StackTrace();
                MethodBase methodBase = stackTrace.GetFrame(1).GetMethod();

                AppLog.LogError(sFormatted, methodBase);
            }
            catch {
                //
            }
        }


        static public String GetLastError() {
            try {
                String value;
                if (falseCauses.TryGetValue(GetCurrentWin32ThreadId(), out value))
                    return value;
                else
                    return String.Empty;
            }
            catch {
                return String.Empty;
            }

        }

        static public void WriteIEnumerableToFile<T>(IEnumerable<T> query, String filename) {
            try {

                StringBuilder sb = new StringBuilder();

                var t = query.Select(l => sb.AppendLine(l.ToString())).ToList();

                File.WriteAllText(filename, sb.ToString(), Encoding.UTF8);

            }
            catch (Exception x) {
                Log(x);
            }
        }
    }
}
