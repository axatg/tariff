﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tariff;

namespace Tariff {
    // корневая view model 
    // содержит ссылки на всю иерархию view model для данной формы
    // формируют/модифицируют view model объекты из сборки BusinessLogic и обратные байнды из юзер-контролов
    public class MainViewModel : INotifyPropertyChanged { 
        public event PropertyChangedEventHandler PropertyChanged; // под-viewModel забайдены в проперти DataContext и фактически данные получат/обновят при изменении данных в этой view model

        

        private RateChangesViewModel rateChangesViewModel; // view model для контрола ввода изменений размера оплаты труда
        private StaffChangesViewModel staffChangesViewModel; // view model для контрола ввода изменений численности штата
        private ReportViewModel reportViewModel; // view model для контрола "отчета"

        public RateChangesViewModel RateChangesViewModel { get { return rateChangesViewModel; } set { rateChangesViewModel = value; OnPropertyChanged("RateChanges"); } }
        public StaffChangesViewModel StaffChangesViewModel { get { return staffChangesViewModel; } set { staffChangesViewModel = value; OnPropertyChanged("StaffChangesViewModel"); } }
        public ReportViewModel ReportViewModel { get { return reportViewModel; } set { reportViewModel = value; OnPropertyChanged("ReportViewModel"); } }

        public MainViewModel(IList<Department> departments, IList<Position> positions) { // при формировании передаются списки отделов и должностей для комбобоксов

            rateChangesViewModel = new RateChangesViewModel(positions); // создаем view model, передаем туда ссылку на список должностей - он нужен для комбобокса
            staffChangesViewModel = new StaffChangesViewModel(departments, positions); // создаем view model, передаем туда ссылки на списки отделов и должностей - они нужны для комбобоксов
            reportViewModel = new ReportViewModel(); // создаем view model для контрола отчета

        }

        protected virtual void OnPropertyChanged(string propertyName) { // стандартная реализация
            try {
                if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

    }
}
