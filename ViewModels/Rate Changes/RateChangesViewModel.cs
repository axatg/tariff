﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    // view model для контрола содержащего всю информацию по изменению размера оплаты труда
    // фактически содержит под-viewModel для контролов "форма добавления" и "список того что есть"

    public class RateChangesViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged; // байдинг

        private RateChangesListViewModel rateChangesListViewModel; // view model списка
        private AddRateChangeViewModel addRateChangeViewModel; // view model формы добавления

        private Boolean addRateChangeIsEnabled; // говорит ui: надо ли дизеблить форму добавления. мы ее дизейблим на период обработки нажатия кнопки "Добавить"

        // доступ к полям можели
        public RateChangesListViewModel RateChangesListViewModel { get { return rateChangesListViewModel; } }
        public AddRateChangeViewModel AddRateChangeViewModel { get { return addRateChangeViewModel; } }
        public Boolean AddRateChangeIsEnabled { get { return addRateChangeIsEnabled; } set { addRateChangeIsEnabled = value; OnPropertyChanged("AddRateChangeIsEnabled"); } }
        // ^доступ к полям можели

        public RateChangesViewModel(IList<Position> positions) { // список должностей нужен форме добавления для создания комбобокса

            rateChangesListViewModel = new RateChangesListViewModel();
            addRateChangeViewModel = new AddRateChangeViewModel(positions);
        }

        protected virtual void OnPropertyChanged(string propertyName) { // стандартная реализация
            try {
                if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

    }
}
