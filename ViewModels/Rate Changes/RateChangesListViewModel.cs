﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    // view model для демонстрации списка уже введенных изменений размера оплаты труда
    public class RateChangesListViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged; // поскольку бандим

        private ObservableCollection<RateChange> rateChanges; // ObservableCollection т.к. добавляемую по "Add" запись не будем перечитывать из БД, а на клиенте добавим в коллекцию и контрол дрлжен новую строчку подхватить
        private IDictionary<Int32, String> positionNameDictionary; // словарь должностей - контрол передаст его конвертеру т.к. в коллекции - id должностей, а не текст

        // доступ к полям модели с реализацией INotifyPropertyChanged
        public ObservableCollection<RateChange> RateChanges { get { return rateChanges;} set {rateChanges = value; OnPropertyChanged("RateChanges"); } }
        public IDictionary<Int32, String> PositionNameDictionary { get { return positionNameDictionary; } set { positionNameDictionary = value; OnPropertyChanged("PositionNameDictionary"); } }
        // ^доступ к полям модели с реализацией INotifyPropertyChanged

        protected virtual void OnPropertyChanged(string propertyName) { // стандартная реализация
            try {
                if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

    }
}
