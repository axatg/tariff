﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    // view model для контрола "форма добавления изменения размера оплаты"
    public class AddRateChangeViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged; // для байдинга
        public event Action<RateChange> AddRateChange; // извещаем контроллер из BusinessLogic о том что нажата кнопка, передаем данные

        private RelayCommand addRateChangeCommand; // команда байндится на нажатие кнопки "Добавить"
        
        private DateTime? started; // сюда байндится дата изменения размера оплаты
        private String salary; // сюда байндится сумма изменения размера оплаты        
        private Position position; // сюда байндится должность из комбобокса

        private IList<Position> positionList; // список должностей для комбобокса

        private String lastError; // ошибки добавления данных демонстрируемые пользователю 

        // ** доступ к полям модели с реализацией INotifyPropertyChanged
        public DateTime? Started { get { return started; } set { started = value; OnPropertyChanged("Started"); } }
        public IList<Position> PositionList { get { return positionList; } set { positionList = value; OnPropertyChanged("PositionList"); } }
        public Position Position { get { return position; } set { position = value; OnPropertyChanged("Position"); } }

        public String Salary { get { return salary; } set { salary = value; OnPropertyChanged("Salary"); } }
        public String LastError { get { return lastError; } set { lastError = value; OnPropertyChanged("LastError"); } }

        public RelayCommand AddRateChangeCommand { get { return addRateChangeCommand; } }
        // ** ^доступ к полям модели с реализацией INotifyPropertyChanged

        public AddRateChangeViewModel(IList<Position> positions) {
            this.positionList = positions; // список должностей для комбобокса
            this.addRateChangeCommand = new RelayCommand(addCommand_Clicked); // подписываемся на нажатие кнопки
        }

        // нажали кнопку "Добавить"
        private void addCommand_Clicked(Object parameter) {
            try {

                if (!ValidateForm()) return; // валидируем поля

                // формируем данные для контроллера
                Int32 positionId = position.ID;
                DateTime started = (DateTime) this.started;
                Decimal salary = Decimal.Parse(this.salary.Trim());

                RateChange newRateChange = new RateChange(positionId, started, salary);
                // ^формируем данные для контроллера

                AddRateChange?.Invoke(newRateChange); // форвадим нажатие кнопки контроллеру, который выполнит всю нужную бизнес-логику

            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        // валидация формы. если что-то не так, вернет false (тем остановит обработку события нажатия на кнопку) и заполнит поле LastError: наше сообщение пользователю
        private Boolean ValidateForm() { 
            try {
                if (position == null) {
                    LastError = "Введите должность";
                    return false;
                }

                if (started == null) {
                    LastError = "Введите дату";
                    return false;
                }

                if (salary == null) {
                    LastError = "Введите ставку";
                    return false;
                }
                salary = salary.Trim();

                if (salary.Length == 0) {
                    LastError = "Введите ставку";
                    return false;
                }

                Decimal resultDecimal;
                Boolean salaryIsDecimal  = Decimal.TryParse(salary, out resultDecimal);
                if (!salaryIsDecimal) {
                    LastError = "Неверная сумма ставки";
                    return false;
                }

                if (resultDecimal <= 0) { 
                    LastError = "Неверная сумма ставки";
                    return false;
                }
                LastError = null;

                return true;
            }
            catch (Exception x) {
                AppLog.Log(x);
                return false;
            }
        }

        protected virtual void OnPropertyChanged(string propertyName) { // стандартная реализация
            try {
                if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }
    }
}
