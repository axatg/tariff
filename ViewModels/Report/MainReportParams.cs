﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    // вспомогательная структура для передачи данных команды о создании отчета от view model к бизнес-логике
    public struct MainReportParams {
        public DateTime LowerBound;
        public DateTime UpperBound;

        public MainReportParams(DateTime lowerBound, DateTime upperBound) {
            this.LowerBound = lowerBound;
            this.UpperBound = upperBound;
        }
    }
}
