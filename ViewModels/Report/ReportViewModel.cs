﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    // view model для контрола "отчета"
    public class ReportViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged; // байдинг

        public event Action<MainReportParams> RemakeReport; // извещаем контроллер из BusinessLogic о том что нажата кнопка и передаем данные. строить отчет будет он
        private IList<CompactReportLine> reportData; // будем обновлять данные целиком, поэтому ObservableCollection не нужна

        private RelayCommand remakeReportCommand;        
        private IDictionary<Int32, String> departmentNameDictionary; // словарь отделов для конвертера: в отчете id отделов, а конвертер их сконвертирует в Name

        private DateTime? lowerBound; // сюда байндится нижняя граница диапазона отчета
        private DateTime? upperBound; // сюда байндится верхняя граница диапазона отчета
        private String lastError; // ошибки создания отчета демонстрируемые пользователю: неверный диапазон отчета, либо невозможность составить отчет из-за недостатка данных в БД (например, есть данные о работниках, но нет данных об размере оплаты)

        // ** доступ к полям модели с реализацией INotifyPropertyChanged
        public IDictionary<Int32, String> DepartmentNameDictionary { get { return departmentNameDictionary; } set { departmentNameDictionary = value; OnPropertyChanged("DepartmentNameDictionary"); } }
        public RelayCommand RemakeReportCommand { get { return remakeReportCommand; } }
        public IList<CompactReportLine> ReportData {
            get { return reportData; }
            set { reportData = value; OnPropertyChanged("ReportData"); }
        }

        public String LastError {
            get { return lastError; }
            set { lastError = value; OnPropertyChanged("LastError"); }
        }
        public DateTime? LowerBound { get { return lowerBound; } set { lowerBound = value; OnPropertyChanged("LowerBound"); } }
        public DateTime? UpperBound { get { return upperBound; } set { upperBound = value; OnPropertyChanged("UpperBound"); } }
        // ** ^доступ к полям модели с реализацией INotifyPropertyChanged


        public ReportViewModel() {
            try {
                this.remakeReportCommand = new RelayCommand(remakeReportCommand_Clicked); // подписываемся на нажатие кнопки
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        // нажали кнопку "Создать отчет"
        private void remakeReportCommand_Clicked(Object obj) {
            try {

                if (!ValidateForm()) return; // валидируем поля

                // формируем данные для контроллера
                MainReportParams reportInterval = new MainReportParams((DateTime)lowerBound, (DateTime) upperBound);
                // ^формируем данные для контроллера

                RemakeReport?.Invoke(reportInterval); // форвадим нажатие кнопки контроллеру, который выполнит всю нужную бизнес-логику
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        // валидация формы. если что-то не так, вернет false (тем остановит обработку события нажатия на кнопку) и заполнит поле LastError: наше сообщение пользователю
        private Boolean ValidateForm() {
            try {

                if (lowerBound == null) {
                    LastError = "Введите дату начала периода";
                    return false;
                }

                if (upperBound == null) {
                    LastError = "Введите дату окончания периода";
                    return false;
                }

                if (lowerBound > upperBound) {
                    LastError = "Дата окончания периода меньше даты начала";
                    return false;
                }

                LastError = null;

                return true;
            }
            catch (Exception x) {
                AppLog.Log(x);
                return false;
            }
        }

        protected virtual void OnPropertyChanged(string propertyName) { // стандартная реализация
            try {
                if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }
    }
}
