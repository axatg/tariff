﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    // view model для демонстрации списка уже введенных изменений численности работников
    public class StaffChangesListViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged; // поскольку бандим

        private ObservableCollection<StaffChange> staffChanges; // ObservableCollection т.к. добавляемую по "Add" запись не будем перечитывать из БД, а на клиенте добавим в коллекцию и контрол дрлжен новую строчку подхватить
        private IDictionary<Int32, String> positionNameDictionary; // словарь должностей - контрол передаст его конвертеру т.к. в коллекции - id должностей, а не текст
        private IDictionary<Int32, String> departmentNameDictionary; // словарь отделов - контрол передаст его конвертеру т.к. в коллекции - id отделов, а не текст

        // доступ к полям модели с реализацией INotifyPropertyChanged
        public ObservableCollection<StaffChange> StaffChanges { get { return staffChanges; } set { staffChanges = value; OnPropertyChanged("StaffChanges"); } }
        public IDictionary<Int32, String> DepartmentNameDictionary { get { return departmentNameDictionary; } set { departmentNameDictionary = value; OnPropertyChanged("DepartmentNameDictionary"); } }
        public IDictionary<Int32, String> PositionNameDictionary { get { return positionNameDictionary; } set { positionNameDictionary = value; OnPropertyChanged("PositionNameDictionary"); } }
        // ^доступ к полям модели с реализацией INotifyPropertyChanged

        protected virtual void OnPropertyChanged(string propertyName) { // стандартная реализация
            try {
                if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

    }
}
