﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    // view model для контрола "форма добавления изменения числа работников"
    public class AddStaffChangeViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged; // для байдинга
        public event Action<StaffChange> AddStaffChange; // извещаем контроллер из BusinessLogic о том что нажата кнопка, передаем данные

        private RelayCommand addStaffChangeCommand; // команда байндится на нажатие кнопки "Добавить"

        private DateTime? started; // сюда байндится дата изменения числа работников
        private String number; // сюда байндится число работников        
        private Department department; // сюда байндится отдел из комбобокса
        private Position position; // сюда байндится должность из комбобокса

        private String lastError; // ошибки добавления данных демонстрируемые пользователю 

        private IList<Department> departmentList; // список отделов для комбобокса
        private IList<Position> positionList; // список должностей для комбобокса

        // ** доступ к полям модели с реализацией INotifyPropertyChanged
        public DateTime? Started { get { return started; } set { started = value; OnPropertyChanged("Started"); } }
        public IList<Department> DepartmentList { get { return departmentList; } set { departmentList = value; OnPropertyChanged("DepartmentList"); } }
        public IList<Position> PositionList { get { return positionList; } set { positionList = value; OnPropertyChanged("PositionList"); } }
        public Department Department { get { return department; } set { department = value; OnPropertyChanged("Department"); } }
        public Position Position { get { return position; } set { position = value; OnPropertyChanged("Position"); } }

        public String Number { get { return number; } set { number = value; OnPropertyChanged("Number"); } }
        public String LastError { get { return lastError; } set { lastError = value; OnPropertyChanged("LastError"); } }
        public RelayCommand AddStaffChangeCommand { get { return addStaffChangeCommand; } }
        // ** ^доступ к полям модели с реализацией INotifyPropertyChanged

        public AddStaffChangeViewModel(IList<Department> departments, IList<Position> positions) {
            this.positionList = positions; // список должностей для комбобокса
            this.departmentList = departments; // список отделов для комбобокса
            this.addStaffChangeCommand = new RelayCommand(addCommand_Clicked); // подписываемся на нажатие кнопки
        }

        // нажали кнопку "Добавить"
        private void addCommand_Clicked(Object parameter) {
            try {

                if (!ValidateForm()) return; // валидируем поля

                // формируем данные для контроллера
                Int32 departmentId = department.ID;
                Int32 positionId = position.ID;
                DateTime started = (DateTime)this.started;
                Int32 number = Int32.Parse(this.number.Trim());

                StaffChange newStaffChange = new StaffChange(departmentId, positionId, started, number);
                // ^формируем данные для контроллера

                AddStaffChange?.Invoke(newStaffChange); // форвадим нажатие кнопки контроллеру, который выполнит всю нужную бизнес-логику

            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private Boolean ValidateForm() { // валидация формы. если что-то не так, вернет false (тем остановит обработку события нажатия на кнопку) и заполнит поле LastError: наше сообщение пользователю
            try {
                if (department == null) {
                    LastError = "Введите отдел";
                    return false;
                }

                if (position == null) {
                    LastError = "Введите должность";
                    return false;
                }

                if (started == null) {
                    LastError = "Введите дату";
                    return false;
                }

                if (number == null) {
                    LastError = "Введите количество сотрудников";
                    return false;
                }
                number = number.Trim();

                if (number.Length == 0) {
                    LastError = "Введите количество сотрудников";
                    return false;
                }

                Int32 resultInt32;
                Boolean numberIsInt32 = Int32.TryParse(number, out resultInt32);
                if (!numberIsInt32) {
                    LastError = "Неверное количество сотрудников";
                    return false;
                }

                if (resultInt32 < 0) {
                    LastError = "Неверное количество сотрудников";
                    return false;
                }
                LastError = null;

                return true;
            }
            catch (Exception x) {
                AppLog.Log(x);
                return false;
            }
        }

        protected virtual void OnPropertyChanged(string propertyName) { // стандартная реализация
            try {
                if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }
    }
}
