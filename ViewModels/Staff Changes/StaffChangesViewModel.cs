﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    // view model для контрола содержащего всю информацию по изменению размера численности работников
    // фактически содержит под-viewModel для контролов "форма добавления" и "список того что есть"
    public class StaffChangesViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged; // байдинг

        private StaffChangesListViewModel staffChangesListViewModel; // view model списка
        private AddStaffChangeViewModel addStaffChangeViewModel; // view model формы добавления

        private Boolean addStaffChangeIsEnabled; // говорит ui: надо ли дизеблить форму добавления. мы ее дизейблим на период обработки нажатия кнопки "Добавить"

        public StaffChangesListViewModel StaffChangesListViewModel { get { return staffChangesListViewModel; } }
        public AddStaffChangeViewModel AddStaffChangeViewModel { get { return addStaffChangeViewModel; } }
        public Boolean AddStaffChangeIsEnabled { get { return addStaffChangeIsEnabled; } set { addStaffChangeIsEnabled = value; OnPropertyChanged("AddStaffChangeIsEnabled"); } }

        public StaffChangesViewModel(IList<Department> departments, IList<Position> positions) { // списки отделов и должностей нужны форме добавления для создания комбобоксов

            staffChangesListViewModel = new StaffChangesListViewModel();
            addStaffChangeViewModel = new AddStaffChangeViewModel(departments, positions);
        }

        protected virtual void OnPropertyChanged(string propertyName) { // стандартная реализация
            try {
                if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

    }
}
