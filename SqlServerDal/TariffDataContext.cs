﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    class TariffDataContext : DbContext {

        public TariffDataContext() : base("MyConnectionString") {
            try {
                
                if (!Database.Exists()) {
                    throw new Exception(String.Format("Нет соединения с базой данных. ({0})" , ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString));
                }

                Boolean isCompatible = Database.CompatibleWithModel(false);
                if (!isCompatible) {                 {
                    throw new Exception("Database is not compatible with model");
                }
            }

            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }

        }

        public DbSet<Department> Departments { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<RateChange> RateChanges { get; set; }
        public DbSet<StaffChange> StaffChanges { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {

            modelBuilder.Entity<Department>().ToTable("department");
            modelBuilder.Entity<Department>().HasKey(a => a.ID);
            modelBuilder.Entity<Department>().Property(a => a.Name).HasColumnName("name").HasMaxLength(255).IsRequired();

            modelBuilder.Entity<Position>().ToTable("position");
            modelBuilder.Entity<Position>().HasKey(a => a.ID);
            modelBuilder.Entity<Position>().Property(a => a.ID).HasColumnName("id").IsRequired();
            modelBuilder.Entity<Position>().Property(a => a.Name).HasColumnName("name").HasMaxLength(255).IsRequired();

            modelBuilder.Entity<RateChange>().ToTable("rate_change");
            modelBuilder.Entity<RateChange>().HasKey(a => a.ID);
            modelBuilder.Entity<RateChange>().Property(a => a.PositionId).HasColumnName("position_id").IsRequired();
            modelBuilder.Entity<RateChange>().Property(a => a.Salary).HasColumnName("salary").HasPrecision(15, 2).IsRequired();
            modelBuilder.Entity<RateChange>().Property(a => a.Started).HasColumnName("started").IsRequired();

            modelBuilder.Entity<StaffChange>().ToTable("staff_change");
            modelBuilder.Entity<StaffChange>().HasKey(a => a.ID);
            modelBuilder.Entity<StaffChange>().Property(a => a.PositionId).HasColumnName("position_id").IsRequired();
            modelBuilder.Entity<StaffChange>().Property(a => a.DepartmentId).HasColumnName("department_id").IsRequired();
            modelBuilder.Entity<StaffChange>().Property(a => a.Started).HasColumnName("started").IsRequired();
            modelBuilder.Entity<StaffChange>().Property(a => a.Number).HasColumnName("number").IsRequired();
        }
    }
}
