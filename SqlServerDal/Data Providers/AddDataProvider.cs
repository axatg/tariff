﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    public class AddDataProvider {

        public void Add(RateChange rateChange) {
            try {
                using (var dc = new TariffDataContext()) {
                    dc.RateChanges.Add(rateChange);
                    dc.SaveChanges();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public void Add(Position position) {
            try {
                using (var dc = new TariffDataContext()) {
                    dc.Positions.Add(position);
                    dc.SaveChanges();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public Boolean RateChangeIsAlreadyExist(Int32 positionId, DateTime started) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.RateChanges.Where(l=>l.PositionId == positionId && l.Started == started).Count() > 0;                    
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public Boolean StaffChangeIsAlreadyExist(Int32 departmentId, Int32 positionId, DateTime started) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.StaffChanges.Where(l => l.DepartmentId == departmentId && l.PositionId == positionId && l.Started == started).Count() > 0;
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public void Add(Department department) {
            try {
                using (var dc = new TariffDataContext()) {
                    dc.Departments.Add(department);
                    dc.SaveChanges();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public void Add(StaffChange staffChange) {
            try {
                using (var dc = new TariffDataContext()) {
                    dc.StaffChanges.Add(staffChange);
                    dc.SaveChanges();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

    }
}
