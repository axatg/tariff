﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    public class StaffChangesDataProvider {

        // возвращает список позиций в департаменте
        public IList<Int32> DepartmentHelper_GetDepartmentPositions(Int32 departmentId, DateTime upperDate) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.StaffChanges
                        .Where(l => l.DepartmentId == departmentId && l.Started <= upperDate)
                        .Select(l => l.PositionId)
                        .Distinct()
                        .ToList();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        // возвращает строку имеющую дату изменения равную началу периода или первую предшествующую ей
        public StaffChange PositionHelper_GetActualLowerDateChange(Int32 departmentId, Int32 positionId, DateTime lowerDate) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.StaffChanges
                        .Where(l => l.PositionId == positionId && l.DepartmentId == departmentId && l.Started <= lowerDate)
                        .OrderByDescending(l => l.Started)
                        .FirstOrDefault();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        // возвращает строку имеющую самую раннюю дату изменения
        public StaffChange PositionHelper_GetLowerDateChange(Int32 departmentId, Int32 positionId, DateTime upperDate) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.StaffChanges
                        .Where(l => l.PositionId == positionId && l.DepartmentId == departmentId && l.Number > 0 && l.Started <= upperDate)
                        .OrderBy(l => l.Started)
                        .FirstOrDefault();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        // возвращает список изменений штата за период
        public IList<StaffChange> PositionHelper_GetOrderedStaffChanges(Int32 departmentId, Int32 positionId, DateTime lower, DateTime upper) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.StaffChanges
                        .Where(l => l.PositionId == positionId && l.DepartmentId == departmentId && l.Started >= lower && l.Started <= upper)
                        .OrderBy(l => l.Started)
                        .ToList();                        
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        // список интересующих нас позиций: те что стартовали до upperDate включительно (стартовали с составом в 0 человек не считаются)
        public IList<Int32> Reporter_PotentialPositions(DateTime upperDate) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.StaffChanges
                        .Where(l => l.Started <= upperDate && l.Number > 0)
                        .Select(l => l.PositionId)
                        .Distinct()
                        .ToList();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        // составляем список интересующих нас департаментов: те что стартовали до upperDate включительно (стартовали с составом в 0 человек не считаются)
        public IList<Int32> Reporter_PotentialDepartments(DateTime upperDate) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.StaffChanges
                        .Where(l => l.Started <= upperDate && l.Number > 0)
                        .Select(l => l.DepartmentId)
                        .Distinct()
                        .ToList();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }
    }
}
