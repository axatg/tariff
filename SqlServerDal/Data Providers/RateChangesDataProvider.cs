﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    public class RateChangesDataProvider {

        // возвращает строку имеющую дату изменения равную началу периода или первую предшествующую ей
        public RateChange PositionRateHelper_GetActualLowerDateChange(Int32 positionId, DateTime lowerDate) {
            try {
                using (var dc = new TariffDataContext()) {
                    IQueryable<RateChange> allPositionRateChanges = dc.RateChanges.Where(l => l.PositionId == positionId);

                    return allPositionRateChanges.Where(l => l.Started <= lowerDate).OrderByDescending(l => l.Started).FirstOrDefault();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public RateChange PositionRateHelper_GetLowerDateChange(Int32 positionId, DateTime upperDate) {
            try {
                using (var dc = new TariffDataContext()) {
                    IQueryable<RateChange> allPositionRateChanges = dc.RateChanges.Where(l => l.PositionId == positionId && l.Started<= upperDate);

                    return allPositionRateChanges.OrderBy(l => l.Started).FirstOrDefault(); 
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        // составляем упорядоченный справочник изменений по должности
        public IList<RateChange> PositionRateHelper_GetOrderedRateChanges(Int32 positionId, DateTime lower, DateTime upper) {
            try {
                using (var dc = new TariffDataContext()) {
                    IQueryable<RateChange> allPositionRateChanges = dc.RateChanges.Where(l => l.PositionId == positionId);

                    return allPositionRateChanges.Where(l => l.Started >= lower && l.Started <= upper).OrderBy(l => l.Started).ToList();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }
    }
}
