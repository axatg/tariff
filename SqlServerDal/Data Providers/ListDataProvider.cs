﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    public class ListDataProvider {

        public IList<Position> GetPositions() {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.Positions.ToList();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public Int32 GetPositionsCount() {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.Positions.Count();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public IList<Department> GetDepartments() {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.Departments.ToList();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public ObservableCollection<RateChange> GetRateChanges() {
            try {
                using (var dc = new TariffDataContext()) {
                    return new ObservableCollection<RateChange>(dc.RateChanges);
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public ObservableCollection<StaffChange> GetStaffChanges() {
            try {
                using (var dc = new TariffDataContext()) {
                    return new ObservableCollection<StaffChange>(dc.StaffChanges);
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public String PositionNameById(Int32 positionId) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.Positions.Where(l=>l.ID == positionId).First().Name;
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        public String DepartmentNameById(Int32 departmentId) {
            try {
                using (var dc = new TariffDataContext()) {
                    return dc.Departments.Where(l => l.ID == departmentId).First().Name;
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }
    }
}
