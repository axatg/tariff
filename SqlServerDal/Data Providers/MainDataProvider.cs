﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tariff;
using System.Collections.ObjectModel;

namespace Tariff {

    public class MainDataProvider {
        //tariffData
        
        private TariffDataContext dataContext;

        private AddDataProvider addDataProvider;
        private ListDataProvider listDataProvider;
        private StaffChangesDataProvider staffChangesDataProvider;
        private RateChangesDataProvider rateChangesDataProvider;

        public AddDataProvider AddDataProvider { get { return addDataProvider; } }
        public ListDataProvider ListDataProvider { get { return listDataProvider; } }
        public StaffChangesDataProvider StaffChangesDataProvider { get { return staffChangesDataProvider; } }
        public RateChangesDataProvider RateChangesDataProvider { get { return rateChangesDataProvider; } }

        public MainDataProvider() {
            try {
                ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings["MyConnectionString"];
                String connectionString = connectionStringSettings.ConnectionString;

                dataContext = new TariffDataContext();
                dataContext.Database.Log = s => AppLog.LogTrace("dataContext: {0}", s);
                
                Boolean connectedSuccessfully = dataContext.Database.Exists();

                addDataProvider = new AddDataProvider();
                listDataProvider = new ListDataProvider();
                staffChangesDataProvider = new StaffChangesDataProvider();
                rateChangesDataProvider = new RateChangesDataProvider();
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }


   
    }
}
