﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff.Mappings {
    class DepartmentMapping : EntityTypeConfiguration<Department> {
        public DepartmentMapping() {
            ToTable("department");
            HasKey(a => a.ID);
            Property(a => a.Name).HasColumnName("name").HasMaxLength(255).IsRequired();           
        }
    }
}
