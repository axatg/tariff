﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff.Mappings {
    class RateChangeMapping : EntityTypeConfiguration<RateChange> {
        public RateChangeMapping() {
            ToTable("rate_change");
            HasKey(a => a.ID);
            Property(a => a.PositionId).HasColumnName("position_id").IsRequired();
            Property(a => a.Salary).HasColumnName("salary").HasPrecision(15, 2).IsRequired();
            Property(a => a.Started).HasColumnName("started").IsRequired();
        }
    }
}
