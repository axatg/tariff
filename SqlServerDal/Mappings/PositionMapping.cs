﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff.Mappings {
    public class PositionMapping : EntityTypeConfiguration<Position> {
        public PositionMapping() {
            ToTable("position");
            HasKey(a => a.ID);
            Property(a => a.Name).HasColumnName("name").HasMaxLength(255).IsRequired();
        }
    }
}
