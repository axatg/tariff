﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff.Mappings {
    class StaffChangeMapping : EntityTypeConfiguration<StaffChange> {
        public StaffChangeMapping() {
            ToTable("rate_change");
            HasKey(a => a.ID);
            Property(a => a.PositionId).HasColumnName("position_id").IsRequired();
            Property(a => a.DepartmentId).HasColumnName("department_id").IsRequired();
            Property(a => a.Started).HasColumnName("started").IsRequired();
            Property(a => a.Number).HasColumnName("number").IsRequired();
        }
    }
}
