﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tariff;

namespace Tariff {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            MainController appManager;

            try {
                InitializeComponent();

                String logFolder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                AppLog.Init(logFolder, "tariff");
                AppLog.LogTrace("Tariff started");

                appManager = new MainController(this.mainControl);                

            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }
    }
}
