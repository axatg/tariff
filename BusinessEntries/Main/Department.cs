﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    [Serializable] // базовые классы модели данных обязательно сериализуются - в будущем пригодится
    public class Department {
        public Int32 ID { get; set; }
        public String Name { get; set; }

        public Department() { } // нужен для ef

        public Department(String Name) {
            this.Name = Name;
        }

    }
}
