﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    [Serializable]
    public class StaffChange { // базовые классы модели данных обязательно сериализуются - в будущем пригодится
        public Int32 ID { get; set; }
        public Int32 DepartmentId { get; set; }
        public Int32 PositionId { get; set; }
        public DateTime Started { get; set; }
        public Int32 Number { get; set; }

        public StaffChange() { } // нужен для ef

        public StaffChange(Int32 departmentId, Int32 positionId, DateTime started, Int32 number) {
            this.DepartmentId = departmentId;
            this.PositionId = positionId;
            this.Started = started;
            this.Number = number;
        }
    }
}
