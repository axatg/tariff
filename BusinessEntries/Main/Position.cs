﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    [Serializable] // базовые классы модели данных обязательно сериализуются - в будущем пригодится
    public class Position
    {
        [Key]
        public Int32 ID { get; set; }
        public String Name { get; set; }

        public Position() { } // нужен для ef

        public Position(String Name) {
            this.Name = Name;
        }

    }





}
