﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    [Serializable]
    public class RateChange { // базовые классы модели данных обязательно сериализуются - в будущем пригодится
        public Int32 ID { get; set; }
        public Int32 PositionId { get; set; }
        public DateTime Started { get; set; }
        public Decimal Salary { get; set; }

        public RateChange() { } // нужен для ef

        public RateChange(Int32 positionId, DateTime started, Decimal salary) {
            this.PositionId = positionId;
            this.Started = started;
            this.Salary = salary;
        }
    }
}
