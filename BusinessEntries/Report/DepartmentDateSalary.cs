﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    public struct DepartmentDateSalary { // структура для создания вспомогательного отчета - помесячной версии отчета "Report"
        public Int32 DepartmentId;
        public DateTime Date;
        public Decimal Salary;

        public DepartmentDateSalary(Int32 departmentId, DateTime date, Decimal salary) {
            this.DepartmentId = departmentId;
            this.Date = date;
            this.Salary = salary;
        }

        public override String ToString() { // только для отладки
            return String.Format("{0}, {1}, {2}", DepartmentId, Date, Salary);
        }
    }
}
