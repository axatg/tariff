﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    public struct CompactReportLine { // структура для создания основного отчета "Report"
        public Int32 DepartmentId { get; set; }
        public DateTime LowerDate { get; set; }
        public DateTime UpperDate { get; set; }
        public Decimal Salary { get; set; }

        public CompactReportLine(Int32 departmentId, DateTime lowerDate, DateTime upperDate, Decimal salary) {
            this.DepartmentId = departmentId;
            this.LowerDate = lowerDate;
            this.UpperDate = upperDate;
            this.Salary = salary;
        }

        public override String ToString() { // для отладочных целей
            return String.Format("{0}, {1}, {2}, {3}", DepartmentId, LowerDate, UpperDate, Salary);
        }
    }
}
