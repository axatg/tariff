﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tariff;

namespace Tariff
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MainControl : UserControl
    {
        public MainControl()
        {
            InitializeComponent();
            this.DataContextChanged += MainControl_DataContextChanged;            
        }

        private void MainControl_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e) {
            try {

                MainViewModel mainViewModel = this.DataContext as MainViewModel;
                
                // ***                

                TabItem ratesTabItem = new TabItem();
                ratesTabItem.Header = "Изменения ставки";

                RateChangesControl rateChangesControl = new RateChangesControl();
                ratesTabItem.Content = rateChangesControl;

                Bind("RateChangesViewModel", rateChangesControl, DataContextProperty, BindingMode.OneWay);                
                
                // ***

                TabItem staffTabItem = new TabItem();
                staffTabItem.Header = "Изменения штатного расписания";

                StaffChangesControl staffChangesControl = new StaffChangesControl();
                staffTabItem.Content = staffChangesControl;
                Bind("StaffChangesViewModel", staffChangesControl, DataContextProperty, BindingMode.OneWay);

                // ***

                TabItem reportTabItem = new TabItem();
                reportTabItem.Header = "Отчет";

                ReportControl reportControl = new ReportControl();
                reportTabItem.Content = reportControl;

                Bind("ReportViewModel", reportControl, DataContextProperty, BindingMode.OneWay);
                
                // ***

                TabControl tabControl = new TabControl();
                tabControl.Items.Add(ratesTabItem);
                tabControl.Items.Add(staffTabItem);
                tabControl.Items.Add(reportTabItem);

                this.Content = tabControl;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void Bind(String vmPropertyName, FrameworkElement vObject, DependencyProperty vProperty, BindingMode mode) {
            Binding binding = new Binding(vmPropertyName);
            binding.Mode = mode;
            vObject.SetBinding(vProperty, binding);
        }
    }
}
