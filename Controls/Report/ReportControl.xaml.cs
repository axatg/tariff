﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tariff {
    /// <summary>
    /// Interaction logic for ReportControl.xaml
    /// </summary>
    public partial class ReportControl : UserControl {
        public ReportControl() {
            InitializeComponent();
            this.DataContextChanged += ReportControl_DataContextChanged;
        }

        private void ReportControl_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e) {
            try {
                ReportViewModel reportViewModel = this.DataContext as ReportViewModel;

                Thickness defaultMargin = new Thickness(5, 0, 10, 0); // в реальном проекте зачитывается из dp верхнего ui элемента

                Grid mainGrid = new Grid();
                mainGrid.Margin = defaultMargin;

                mainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(60, GridUnitType.Star) });
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(40, GridUnitType.Star) });

                DataGrid dataGrid = new DataGrid();
                dataGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                dataGrid.VerticalAlignment = VerticalAlignment.Stretch;                

                dataGrid.AutoGenerateColumns = false;
                dataGrid.CanUserAddRows = false;

                Bind("ReportData", dataGrid, DataGrid.ItemsSourceProperty, BindingMode.OneWay);

                // ***
                Binding departmentBinding = new Binding("DepartmentId");
                departmentBinding.Mode = BindingMode.OneWay;
                departmentBinding.Converter = new IdToNameConverter();
                departmentBinding.ConverterParameter = reportViewModel.DepartmentNameDictionary;

                DataGridTextColumn departmentColumn = new DataGridTextColumn();
                departmentColumn.Binding = departmentBinding;
                departmentColumn.Header = "Отдел";

                dataGrid.Columns.Add(departmentColumn);

                // ***
                Binding lowerDateBinding = new Binding("LowerDate");
                lowerDateBinding.Mode = BindingMode.OneWay;
                lowerDateBinding.StringFormat = "dd.MM.yyyy";

                DataGridTextColumn lowerDateColumn = new DataGridTextColumn();
                lowerDateColumn.Header = "с";
                lowerDateColumn.Binding = lowerDateBinding;

                dataGrid.Columns.Add(lowerDateColumn);

                // ***
                Binding upperDateBinding = new Binding("UpperDate");
                upperDateBinding.Mode = BindingMode.OneWay;
                upperDateBinding.StringFormat = "dd.MM.yyyy";

                DataGridTextColumn upperDateColumn = new DataGridTextColumn();
                upperDateColumn.Header = "по";
                upperDateColumn.Binding = upperDateBinding;

                dataGrid.Columns.Add(upperDateColumn);

                // ***

                Binding salaryBinding = new Binding("Salary");
                salaryBinding.Mode = BindingMode.OneWay;
                salaryBinding.StringFormat = "N2";
                

                DataGridTextColumn salaryColumn = new DataGridTextColumn();
                salaryColumn.Header = "ФОТ отдела в месяц";
                salaryColumn.Binding = salaryBinding;
                

                dataGrid.Columns.Add(salaryColumn);


                // ***
                Grid.SetColumn(dataGrid, 0);
                mainGrid.Children.Add(dataGrid);

                // ***

                StackPanel formStackPanel = new StackPanel();
                formStackPanel.Margin = defaultMargin;
                formStackPanel.Orientation = Orientation.Vertical;

                Label makeReportLabel = new Label();
                makeReportLabel.Content = "Создать отчет";
                formStackPanel.Children.Add(makeReportLabel);
                    
                Label lowerBoundLabel = new Label();
                lowerBoundLabel.Content = "Начало периода:";
                formStackPanel.Children.Add(lowerBoundLabel);

                TariffMonthPickerControl lowerBoundPickerControl = new TariffMonthPickerControl();
                Bind("LowerBound", lowerBoundPickerControl, TariffMonthPickerControl.PickedDateProperty, BindingMode.OneWayToSource);
                formStackPanel.Children.Add(lowerBoundPickerControl);

                Label upperBoundLabel = new Label();
                upperBoundLabel.Content = "Окончание периода:";
                formStackPanel.Children.Add(upperBoundLabel);

                TariffMonthPickerControl upperBoundPickerControl = new TariffMonthPickerControl();
                Bind("UpperBound", upperBoundPickerControl, TariffMonthPickerControl.PickedDateProperty, BindingMode.OneWayToSource);
                formStackPanel.Children.Add(upperBoundPickerControl);

                TextBlock lastErrorTextBlock = new TextBlock(); // TextBlock, а не Label т.к. нужен wrap
                lastErrorTextBlock.TextWrapping = TextWrapping.WrapWithOverflow;
                lastErrorTextBlock.Foreground = new SolidColorBrush(Colors.Red); // цветовую схему нужно зачитывать из DP верхнего UI элемента. Здесь упрощенно
                
                Bind("LastError", lastErrorTextBlock, TextBlock.TextProperty, BindingMode.OneWay);

                formStackPanel.Children.Add(lastErrorTextBlock);

                Button remakeReportButton = new Button();                
                remakeReportButton.Content = "Создать";
                remakeReportButton.HorizontalAlignment = HorizontalAlignment.Left;
                //remakeReportButton.VerticalAlignment = VerticalAlignment.Top;
                formStackPanel.Children.Add(remakeReportButton);

                CommandBinding remakeReportButtonCommandBinding = new CommandBinding();
                remakeReportButton.CommandBindings.Add(remakeReportButtonCommandBinding);
                remakeReportButton.Command = reportViewModel.RemakeReportCommand;

                Grid.SetColumn(formStackPanel, 1);
                mainGrid.Children.Add(formStackPanel);

                Content = mainGrid;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void Bind(String vmPropertyName, FrameworkElement vObject, DependencyProperty vProperty, BindingMode mode) {
            Binding binding = new Binding(vmPropertyName);
            binding.Mode = mode;
            vObject.SetBinding(vProperty, binding);
        }
    }
}
