﻿using CommonClassLibrary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Tariff {
    public class IdToNameConverter : IValueConverter {
        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture) {
            try {

                if (value == null) return null;
                if (parameter == null) return null;

                Int32 id = (Int32)value;                

                Dictionary<Int32, String> dictionary = parameter as Dictionary<Int32, String>;

                String name;
                if (dictionary.TryGetValue(id, out name)) {
                    return name;
                }

                return null;
            }
            catch (Exception x) {
                AppLog.Log(x);
                return null;
            }
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
