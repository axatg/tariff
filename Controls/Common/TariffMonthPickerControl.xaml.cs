﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tariff {
     
    // контрол для ввода даты в формате "месяц-год"

    public partial class TariffMonthPickerControl : UserControl {
        private ComboBox monthsComboBox; // при поступлении команды "очистить форму" потребуется "сбросить в дефолт", для этого храним ссылку
        private ComboBox yearsCombobox; // при поступлении команды "очистить форму" потребуется "сбросить в дефолт", для этого храним ссылку

        public TariffMonthPickerControl() {
            try {
                InitializeComponent();
                MakeControl();
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        // пропертя которая будет байндиться двусторонне UI<->view model: 
        // UI -> view model возвращать выбранное значение
        // view model => UI клиент будет присваивать null при сбросе формы в дефолт

        // если выбраны и месяц и год, в проперте будет дата (1 число введенного месяца)
        // если год или месяц не выбраны, в проперте будет null
        public static readonly DependencyProperty PickedDateProperty = DependencyProperty.RegisterAttached("PickedDate", typeof(DateTime?), typeof(TariffMonthPickerControl), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.Inherits, new PropertyChangedCallback(TariffDatePickerChanged)));


        // возовется когда пользователь wpf засек изменение значения PickedDateProperty
        private static void TariffDatePickerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            try {

                TariffMonthPickerControl tariffMonthPickerControl = d as TariffMonthPickerControl; // метод статичный. так что получаем экземпляр контрола

                if (tariffMonthPickerControl == null) return; // не получили - уходим

                DateTime? newDate = (DateTime?) e.NewValue; // получаем реальное значение

                if (newDate == null) { // от view model пришла команда "сбросить в дефолт" контрол
                    tariffMonthPickerControl.monthsComboBox.SelectedItem = null; // "сбрасываем" selected value комбобокса
                    tariffMonthPickerControl.yearsCombobox.SelectedItem = null; // "сбрасываем" selected value комбобокса
                }
                else { 
                    tariffMonthPickerControl.monthsComboBox.SelectedIndex = newDate.Value.Month - 1;
                    tariffMonthPickerControl.yearsCombobox.SelectedItem = newDate.Value.Year.ToString();
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
            
        }

        public DateTime? PickedDate { // привычный удобный доступ к одноименной DependencyProperty
            get {
                return (DateTime?) GetValue(PickedDateProperty);
            }
            set {
                SetValue(PickedDateProperty, value);                
            }
        }


        private void MakeControl() { // создаем тело контрола
            try {

                var monthNames = new []{ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" };
                
                monthsComboBox = new ComboBox();
                monthsComboBox.Width = 90; 

                foreach (var month in monthNames) {
                    monthsComboBox.Items.Add(month);
                }                
                monthsComboBox.SelectionChanged += MonthsComboBox_SelectionChanged;

                yearsCombobox = new ComboBox();
                yearsCombobox.Width = 60;

                for (var year = 2000; year <= 2030; year++) {
                    yearsCombobox.Items.Add(year.ToString());
                }
                yearsCombobox.SelectionChanged += YearsCombobox_SelectionChanged;

                StackPanel mainStackPanel = new StackPanel();
                mainStackPanel.Orientation = Orientation.Horizontal;

                mainStackPanel.Children.Add(monthsComboBox);
                mainStackPanel.Children.Add(yearsCombobox);

                this.Content = mainStackPanel;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void YearsCombobox_SelectionChanged(Object sender, SelectionChangedEventArgs e) {
            SelectionChanged();
        }

        private void MonthsComboBox_SelectionChanged(Object sender, SelectionChangedEventArgs e) {
            SelectionChanged();
        }

        // пользователь сменил значение в комбобоксе месяца или года
        private void SelectionChanged() {
            try {

                if (monthsComboBox.SelectedItem == null) { return; } // если оба комбобокса не со значением, то считаем что дата еще не выбрана
                if (yearsCombobox.SelectedItem == null) { return; } // если оба комбобокса не со значением, то считаем что дата еще не выбрана

                PickedDate = new DateTime(Int32.Parse((String)yearsCombobox.SelectedItem), monthsComboBox.SelectedIndex + 1, 1); // в соответствии со значениями комбобоксов апдейтим DependencyProperty
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }
    }
}
