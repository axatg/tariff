﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tariff {
    /// <summary>
    /// Interaction logic for AddStaffChangeControl.xaml
    /// </summary>
    public partial class AddStaffChangeControl : UserControl {
        public AddStaffChangeControl() {
            InitializeComponent();
            this.DataContextChanged += AddStaffChangeControl_DataContextChanged;
        }

        private void AddStaffChangeControl_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e) {
            try {
                AddStaffChangeViewModel addStaffChangeViewModel = this.DataContext as AddStaffChangeViewModel;

                StackPanel mainStackPanel = new StackPanel();
                mainStackPanel.Orientation = Orientation.Vertical;


                Label addStaffChangeLabel = new Label();

                addStaffChangeLabel.Padding = new Thickness(0);
                addStaffChangeLabel.Content = "Добавить изменение ставки:";

                mainStackPanel.Children.Add(addStaffChangeLabel);

                // ***

                Label departmentLabel = new Label();
                departmentLabel.Content = "Отдел:";
                mainStackPanel.Children.Add(departmentLabel);

                ComboBox departmentComboBox = new ComboBox();                
                Bind("DepartmentList", departmentComboBox, ComboBox.ItemsSourceProperty, BindingMode.OneWay);                
                Bind("Department", departmentComboBox, ComboBox.SelectedItemProperty, BindingMode.TwoWay);
                departmentComboBox.DisplayMemberPath = "Name";

                mainStackPanel.Children.Add(departmentComboBox);

                // ***

                Label positionLabel = new Label();
                positionLabel.Content = "Должность:";
                mainStackPanel.Children.Add(positionLabel);

                ComboBox positionComboBox = new ComboBox();                
                Bind("PositionList", positionComboBox, ComboBox.ItemsSourceProperty, BindingMode.OneWay);                
                Bind("Position", positionComboBox, ComboBox.SelectedItemProperty, BindingMode.TwoWay);
                positionComboBox.DisplayMemberPath = "Name";

                mainStackPanel.Children.Add(positionComboBox);

                // ***

                Label startedLabel = new Label();
                startedLabel.Content = "Дата начала действия:";
                mainStackPanel.Children.Add(startedLabel);

                TariffMonthPickerControl startedMonthPickerControl = new TariffMonthPickerControl();
                Bind("Started", startedMonthPickerControl, TariffMonthPickerControl.PickedDateProperty, BindingMode.TwoWay);

                mainStackPanel.Children.Add(startedMonthPickerControl);

                Label numberLabel = new Label();
                numberLabel.Content = "Количество сотрудников:";
                mainStackPanel.Children.Add(numberLabel);

                TextBox numberTextBox = new TextBox();
                Bind("Number", numberTextBox, TextBox.TextProperty, BindingMode.TwoWay);

                mainStackPanel.Children.Add(numberTextBox);

                TextBlock lastErrorTextBlock = new TextBlock(); // TextBlock, а не Label т.к. нужен wrap
                lastErrorTextBlock.TextWrapping = TextWrapping.WrapWithOverflow;
                lastErrorTextBlock.Foreground = new SolidColorBrush(Colors.Red); // цветовую схему нужно зачитывать из DP верхнего UI элемента. Здесь упрощенно

                Bind("LastError", lastErrorTextBlock, TextBlock.TextProperty, BindingMode.OneWay);
                mainStackPanel.Children.Add(lastErrorTextBlock);

                Button addButton = new Button();
                addButton.Content = "Добавить";
                addButton.HorizontalAlignment = HorizontalAlignment.Left;

                CommandBinding addButtonCommandBinding = new CommandBinding();
                addButton.CommandBindings.Add(addButtonCommandBinding);
                addButton.Command = addStaffChangeViewModel.AddStaffChangeCommand;

                mainStackPanel.Children.Add(addButton);

                this.Content = mainStackPanel;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }
        private void Bind(String vmPropertyName, FrameworkElement vObject, DependencyProperty vProperty, BindingMode mode) {
            Binding binding = new Binding(vmPropertyName);
            binding.Mode = mode;
            vObject.SetBinding(vProperty, binding);
        }

    }

}