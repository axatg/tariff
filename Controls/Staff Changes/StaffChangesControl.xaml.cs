﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tariff {
    /// <summary>
    /// Interaction logic for StaffsControl.xaml
    /// </summary>
    public partial class StaffChangesControl : UserControl {



        public StaffChangesControl() {
            InitializeComponent();
            this.DataContextChanged += StaffChangesControl_DataContextChanged;
        }

        private void StaffChangesControl_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e) {
            try {

                StaffChangesViewModel StaffChangesViewModel = this.DataContext as StaffChangesViewModel;


                Thickness defaultMargin = new Thickness(5, 0, 10, 0); // в реальном проекте зачитывается из dp верхнего ui элемента

                Grid mainGrid = new Grid();
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(60, GridUnitType.Star) });
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(40, GridUnitType.Star) });

                AddStaffChangeControl addStaffChangeControl = new AddStaffChangeControl();
                addStaffChangeControl.HorizontalAlignment = HorizontalAlignment.Stretch;
                addStaffChangeControl.VerticalAlignment = VerticalAlignment.Stretch;
                addStaffChangeControl.Margin = defaultMargin;
                Bind("AddStaffChangeIsEnabled", addStaffChangeControl, IsEnabledProperty, BindingMode.OneWay);

                addStaffChangeControl.DataContext = StaffChangesViewModel.AddStaffChangeViewModel;

                Grid.SetColumn(addStaffChangeControl, 1);
                mainGrid.Children.Add(addStaffChangeControl);

                StaffChangesListControl StaffChangesListControl = new StaffChangesListControl();
                StaffChangesListControl.HorizontalAlignment = HorizontalAlignment.Stretch;
                StaffChangesListControl.VerticalAlignment = VerticalAlignment.Stretch;
                StaffChangesListControl.Margin = defaultMargin;

                StaffChangesListControl.DataContext = StaffChangesViewModel.StaffChangesListViewModel;

                Grid.SetColumn(StaffChangesListControl, 0);
                mainGrid.Children.Add(StaffChangesListControl);

                this.Content = mainGrid;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void Bind(String vmPropertyName, FrameworkElement vObject, DependencyProperty vProperty, BindingMode mode) {
            Binding binding = new Binding(vmPropertyName);
            binding.Mode = mode;
            vObject.SetBinding(vProperty, binding);
        }

    }
}
