﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tariff {
    /// <summary>
    /// Interaction logic for StaffChangesListControl.xaml
    /// </summary>
    public partial class StaffChangesListControl : UserControl {
        public StaffChangesListControl() {
            InitializeComponent();
            this.DataContextChanged += StaffChangesListControl_DataContextChanged;
        }

        private void StaffChangesListControl_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e) {
            try {

                StaffChangesListViewModel StaffChangesListViewModel = this.DataContext as StaffChangesListViewModel;

                DataGrid dataGrid = new DataGrid();
                dataGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                dataGrid.VerticalAlignment = VerticalAlignment.Stretch;



                dataGrid.AutoGenerateColumns = false;
                dataGrid.CanUserAddRows = false;

                Bind("StaffChanges", dataGrid, DataGrid.ItemsSourceProperty, BindingMode.OneWay);

                // *** 
                Binding departmentBinding = new Binding("DepartmentId");
                departmentBinding.Mode = BindingMode.OneWay;
                departmentBinding.Converter = new IdToNameConverter();
                departmentBinding.ConverterParameter = StaffChangesListViewModel.DepartmentNameDictionary;

                DataGridTextColumn departmentColumn = new DataGridTextColumn();
                departmentColumn.Binding = departmentBinding;
                departmentColumn.Header = "Отдел";

                dataGrid.Columns.Add(departmentColumn);

                // ***

                Binding positionBinding = new Binding("PositionId");
                positionBinding.Mode = BindingMode.OneWay;
                positionBinding.Converter = new IdToNameConverter();
                positionBinding.ConverterParameter = StaffChangesListViewModel.PositionNameDictionary;

                DataGridTextColumn positionColumn = new DataGridTextColumn();
                positionColumn.Binding = positionBinding;
                positionColumn.Header = "Должность";

                dataGrid.Columns.Add(positionColumn);

                // *** 
                Binding startedBinding = new Binding("Started");
                startedBinding.Mode = BindingMode.OneWay;
                startedBinding.StringFormat = "dd.MM.yyyy";

                DataGridTextColumn startedColumn = new DataGridTextColumn();
                startedColumn.Header = "Дата";
                startedColumn.Binding = startedBinding;


                dataGrid.Columns.Add(startedColumn);


                // *** 
                Binding numberBinding = new Binding("Number");
                numberBinding.Mode = BindingMode.OneWay;
                numberBinding.StringFormat = "D";

                DataGridTextColumn numberColumn = new DataGridTextColumn();
                numberColumn.Header = "Кол-во сотрудников";
                numberColumn.Binding = numberBinding;

                dataGrid.Columns.Add(numberColumn);

                // ***

                StackPanel mainStackPanel = new StackPanel();
                mainStackPanel.Orientation = Orientation.Vertical;

                mainStackPanel.Children.Add(dataGrid);

                this.Content = mainStackPanel;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void Bind(String vmPropertyName, FrameworkElement vObject, DependencyProperty vProperty, BindingMode mode) {
            Binding binding = new Binding(vmPropertyName);
            binding.Mode = mode;
            vObject.SetBinding(vProperty, binding);
        }
    }
}
