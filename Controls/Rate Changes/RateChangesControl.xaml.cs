﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tariff {
    /// <summary>
    /// Interaction logic for RatesControl.xaml
    /// </summary>
    public partial class RateChangesControl : UserControl {

        

        public RateChangesControl() {
            InitializeComponent();
            this.DataContextChanged += RateChangesControl_DataContextChanged;
        }

        private void RateChangesControl_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e) {
            try {

                RateChangesViewModel rateChangesViewModel = this.DataContext as RateChangesViewModel;                


                Thickness defaultMargin = new Thickness(5, 0, 10, 0); // в реальном проекте зачитывается из dp верхнего ui элемента

                Grid mainGrid = new Grid();
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(60, GridUnitType.Star) } );
                mainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(40, GridUnitType.Star) });                

                AddRateChangeControl addRateChangeControl = new AddRateChangeControl();
                addRateChangeControl.HorizontalAlignment = HorizontalAlignment.Stretch;
                addRateChangeControl.VerticalAlignment = VerticalAlignment.Stretch;
                addRateChangeControl.Margin = defaultMargin;
                Bind("AddRateChangeIsEnabled", addRateChangeControl, IsEnabledProperty, BindingMode.OneWay);

                addRateChangeControl.DataContext = rateChangesViewModel.AddRateChangeViewModel;

                Grid.SetColumn(addRateChangeControl, 1);
                mainGrid.Children.Add(addRateChangeControl);

                RateChangesListControl rateChangesListControl = new RateChangesListControl();
                rateChangesListControl.HorizontalAlignment = HorizontalAlignment.Stretch;
                rateChangesListControl.VerticalAlignment = VerticalAlignment.Stretch;
                rateChangesListControl.Margin = defaultMargin;

                rateChangesListControl.DataContext = rateChangesViewModel.RateChangesListViewModel;

                Grid.SetColumn(rateChangesListControl, 0);
                mainGrid.Children.Add(rateChangesListControl);

                this.Content = mainGrid;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void Bind(String vmPropertyName, FrameworkElement vObject, DependencyProperty vProperty, BindingMode mode) {
            Binding binding = new Binding(vmPropertyName);
            binding.Mode = mode;
            vObject.SetBinding(vProperty, binding);
        }

    }
}
