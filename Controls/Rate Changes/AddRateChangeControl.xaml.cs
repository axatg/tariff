﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tariff {
    /// <summary>
    /// Interaction logic for AddRateChangeControl.xaml
    /// </summary>
    public partial class AddRateChangeControl : UserControl {
        public AddRateChangeControl() {
            InitializeComponent();
            this.DataContextChanged += AddRateChangeControl_DataContextChanged;
        }

        private void AddRateChangeControl_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e) {
            try {
                AddRateChangeViewModel addRateChangeViewModel = this.DataContext as AddRateChangeViewModel;

                StackPanel mainStackPanel = new StackPanel();                
                mainStackPanel.Orientation = Orientation.Vertical;
                

                Label addRateChangeLabel = new Label();
                
                addRateChangeLabel.Padding = new Thickness(0);                
                addRateChangeLabel.Content = "Добавить изменение ставки:";                

                mainStackPanel.Children.Add(addRateChangeLabel);
                

                Label positionLabel = new Label();
                positionLabel.Content = "Должность:";
                mainStackPanel.Children.Add(positionLabel);

                ComboBox positionComboBox = new ComboBox();
                
                Bind("PositionList", positionComboBox, ComboBox.ItemsSourceProperty, BindingMode.OneWay);                
                Bind("Position", positionComboBox, ComboBox.SelectedItemProperty, BindingMode.TwoWay);
                positionComboBox.DisplayMemberPath = "Name";


                mainStackPanel.Children.Add(positionComboBox);

                Label startedLabel = new Label();
                startedLabel.Content = "Дата начала действия:";
                mainStackPanel.Children.Add(startedLabel);

                TariffMonthPickerControl startedMonthPickerControl = new TariffMonthPickerControl();
                Bind("Started", startedMonthPickerControl, TariffMonthPickerControl.PickedDateProperty, BindingMode.TwoWay);

                mainStackPanel.Children.Add(startedMonthPickerControl);

                Label salaryLabel = new Label();
                salaryLabel.Content = "Ставка:";
                mainStackPanel.Children.Add(salaryLabel);

                TextBox salaryTextBox = new TextBox();
                Bind("Salary", salaryTextBox, TextBox.TextProperty, BindingMode.TwoWay);
                
                mainStackPanel.Children.Add(salaryTextBox);

                TextBlock lastErrorTextBlock = new TextBlock(); // TextBlock, а не Label т.к. нужен wrap
                lastErrorTextBlock.TextWrapping = TextWrapping.WrapWithOverflow;
                lastErrorTextBlock.Foreground = new SolidColorBrush(Colors.Red); // цветовую схему нужно зачитывать из DP верхнего UI элемента. Здесь упрощенно

                Bind("LastError", lastErrorTextBlock, TextBlock.TextProperty, BindingMode.OneWay);
                mainStackPanel.Children.Add(lastErrorTextBlock);

                Button addButton = new Button();
                addButton.Content = "Добавить";
                addButton.HorizontalAlignment = HorizontalAlignment.Left;

                CommandBinding addButtonCommandBinding = new CommandBinding();
                addButton.CommandBindings.Add(addButtonCommandBinding);
                addButton.Command = addRateChangeViewModel.AddRateChangeCommand;

                mainStackPanel.Children.Add(addButton);

                this.Content = mainStackPanel;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }
        private void Bind(String vmPropertyName, FrameworkElement vObject, DependencyProperty vProperty, BindingMode mode) {
            Binding binding = new Binding(vmPropertyName);
            binding.Mode = mode;
            vObject.SetBinding(vProperty, binding);
        }

    }

}