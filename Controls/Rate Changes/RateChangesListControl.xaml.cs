﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tariff {
    /// <summary>
    /// Interaction logic for RateChangesListControl.xaml
    /// </summary>
    public partial class RateChangesListControl : UserControl {
        public RateChangesListControl() {
            InitializeComponent();
            this.DataContextChanged += RateChangesListControl_DataContextChanged;
        }

        private void RateChangesListControl_DataContextChanged(Object sender, DependencyPropertyChangedEventArgs e) {
            try {

                RateChangesListViewModel rateChangesListViewModel = this.DataContext as RateChangesListViewModel;

                DataGrid dataGrid = new DataGrid();
                dataGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
                dataGrid.VerticalAlignment = VerticalAlignment.Stretch;

                

                dataGrid.AutoGenerateColumns = false;
                dataGrid.CanUserAddRows = false;

                Bind("RateChanges", dataGrid, DataGrid.ItemsSourceProperty, BindingMode.OneWay);                

                // *** 

                //positionNameConverter

                Binding positionBinding = new Binding("PositionId");
                positionBinding.Mode = BindingMode.OneWay;
                positionBinding.Converter = new IdToNameConverter();
                positionBinding.ConverterParameter = rateChangesListViewModel.PositionNameDictionary;

                DataGridTextColumn positionColumn = new DataGridTextColumn();
                positionColumn.Binding = positionBinding;
                positionColumn.Header = "Должность";

                dataGrid.Columns.Add(positionColumn);

                // *** 
                Binding startedBinding = new Binding("Started");
                startedBinding.Mode = BindingMode.OneWay;
                startedBinding.StringFormat = "dd.MM.yyyy";

                DataGridTextColumn startedColumn = new DataGridTextColumn();
                startedColumn.Header = "Дата";
                startedColumn.Binding = startedBinding;
                

                dataGrid.Columns.Add(startedColumn);


                // *** 
                Binding salaryBinding = new Binding("Salary");
                salaryBinding.Mode = BindingMode.OneWay;
                salaryBinding.StringFormat = "N2";

                DataGridTextColumn salaryColumn = new DataGridTextColumn();
                salaryColumn.Header = "Ставка";
                salaryColumn.Binding = salaryBinding;

                dataGrid.Columns.Add(salaryColumn);

                // ***

                StackPanel mainStackPanel = new StackPanel();
                mainStackPanel.Orientation = Orientation.Vertical;

                mainStackPanel.Children.Add(dataGrid);

                this.Content = mainStackPanel;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void Bind(String vmPropertyName, FrameworkElement vObject, DependencyProperty vProperty, BindingMode mode) {
            Binding binding = new Binding(vmPropertyName);
            binding.Mode = mode;
            vObject.SetBinding(vProperty, binding);
        }
    }
}
