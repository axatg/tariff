﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tariff;
using System.Collections.Generic;

namespace TestUnits {
    [TestClass]
    public class ReporterUT : UTBase {
        [TestMethod]
        public void Reporte_MakeReport() {
            // arrange
            MainDataProvider dal = new MainDataProvider();

            Reporter reporter = new Reporter(dal);

            DateTime lowerBound = new DateTime(2016, 1, 1);
            DateTime upperBound = new DateTime(2016, 12, 1);

            String falseCause;
            IList<CompactReportLine> compactReport;

            // act
            Boolean result = reporter.MakeReport(lowerBound, upperBound, out compactReport, out falseCause);

            // assert
            Assert.IsTrue(result);            

        }
    }
}
