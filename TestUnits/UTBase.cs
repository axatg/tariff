﻿
using CommonClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestUnits {
    public class UTBase {
        [TestInitialize]
        public void Initialize() {
            AppLog.Init(@"D:\C# Projects\Tariff\log", "test");
            AppLog.LogTrace("Test started.");
        }

        [TestCleanup]
        public void Cleanup() {            
            AppLog.LogTrace("Test Complete.");
        }

    }
}
