﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tariff;
using System.Collections.Generic;

namespace TestUnits {
    [TestClass]
    public class SqlServerDalLibraryUT : UTBase {

        [TestMethod]
        public void SqlServerDalLibrary_GetPositions() {
            // arrange
            MainDataProvider dal = new MainDataProvider();

            Position position = new Position("Инженер 6");
            dal.AddDataProvider.Add(position);


            // act
            IList<Position> positions = dal.ListDataProvider.GetPositions();

            // assert
            Assert.IsNotNull(positions);
            Assert.AreNotEqual(0, positions.Count);
            Assert.AreEqual("Инженер", positions[0].Name);
            Assert.AreEqual(1, positions[0].ID);

        }

        [TestMethod]
        public void SqlServerDalLibrary_PositionsCount() {
            // arrange
            Tariff.MainDataProvider dal = new MainDataProvider();
            
            //Position position = new Position("Инженер");
            //position.ID = 1;

            // act
            Int32 positionsCount =  dal.ListDataProvider.GetPositionsCount();

            // assert

            Assert.AreEqual(positionsCount, 0);
        }

        //[TestMethod]
        //public void SqlServerDalLibrary_AddPosition() {
        //    // arrange
        //    MainDataProvider dal = new MainDataProvider();

        //    Position position = new Position("Инженер 3");            

        //    // act
        //    dal.AddDataProvider.Add(position);

        //}
    }
}
