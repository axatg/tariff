USE [master]
GO
/****** Object:  Database [dbTariff]    Script Date: 11/15/2018 5:35:44 PM ******/
CREATE DATABASE [dbTariff]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbTariff', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\dbTariff.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'dbTariff_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\dbTariff_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [dbTariff] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbTariff].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbTariff] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbTariff] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbTariff] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbTariff] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbTariff] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbTariff] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbTariff] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbTariff] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbTariff] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbTariff] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbTariff] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbTariff] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbTariff] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbTariff] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbTariff] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbTariff] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbTariff] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbTariff] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbTariff] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbTariff] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbTariff] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbTariff] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbTariff] SET RECOVERY FULL 
GO
ALTER DATABASE [dbTariff] SET  MULTI_USER 
GO
ALTER DATABASE [dbTariff] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbTariff] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbTariff] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbTariff] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [dbTariff] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'dbTariff', N'ON'
GO
USE [dbTariff]
GO
/****** Object:  Table [dbo].[department]    Script Date: 11/15/2018 5:35:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[department](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[position]    Script Date: 11/15/2018 5:35:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[position](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[rate_change]    Script Date: 11/15/2018 5:35:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rate_change](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[position_id] [int] NOT NULL,
	[started] [date] NOT NULL,
	[salary] [money] NOT NULL,
 CONSTRAINT [PK_Rate] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[staff_change]    Script Date: 11/15/2018 5:35:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[staff_change](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[department_id] [int] NOT NULL,
	[position_id] [int] NOT NULL,
	[started] [date] NOT NULL,
	[number] [int] NOT NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[department] ON 

INSERT [dbo].[department] ([id], [name]) VALUES (1, N'КБ')
INSERT [dbo].[department] ([id], [name]) VALUES (2, N'Технологический отдел')
SET IDENTITY_INSERT [dbo].[department] OFF
SET IDENTITY_INSERT [dbo].[position] ON 

INSERT [dbo].[position] ([id], [name]) VALUES (1, N'Инженер конструктор')
INSERT [dbo].[position] ([id], [name]) VALUES (2, N'Инженер технолог')
SET IDENTITY_INSERT [dbo].[position] OFF
SET IDENTITY_INSERT [dbo].[rate_change] ON 

INSERT [dbo].[rate_change] ([id], [position_id], [started], [salary]) VALUES (16, 1, CAST(N'2015-01-01' AS Date), 10000.0000)
INSERT [dbo].[rate_change] ([id], [position_id], [started], [salary]) VALUES (17, 1, CAST(N'2015-06-01' AS Date), 15000.0000)
INSERT [dbo].[rate_change] ([id], [position_id], [started], [salary]) VALUES (18, 2, CAST(N'2015-01-01' AS Date), 12000.0000)
INSERT [dbo].[rate_change] ([id], [position_id], [started], [salary]) VALUES (19, 2, CAST(N'2015-06-01' AS Date), 17000.0000)
INSERT [dbo].[rate_change] ([id], [position_id], [started], [salary]) VALUES (20, 1, CAST(N'2016-03-01' AS Date), 18000.0000)
SET IDENTITY_INSERT [dbo].[rate_change] OFF
SET IDENTITY_INSERT [dbo].[staff_change] ON 

INSERT [dbo].[staff_change] ([id], [department_id], [position_id], [started], [number]) VALUES (8, 1, 1, CAST(N'2015-01-01' AS Date), 5)
INSERT [dbo].[staff_change] ([id], [department_id], [position_id], [started], [number]) VALUES (9, 1, 1, CAST(N'2015-06-01' AS Date), 10)
INSERT [dbo].[staff_change] ([id], [department_id], [position_id], [started], [number]) VALUES (10, 2, 2, CAST(N'2015-01-01' AS Date), 5)
INSERT [dbo].[staff_change] ([id], [department_id], [position_id], [started], [number]) VALUES (11, 2, 2, CAST(N'2015-06-01' AS Date), 10)
INSERT [dbo].[staff_change] ([id], [department_id], [position_id], [started], [number]) VALUES (12, 1, 2, CAST(N'2018-01-01' AS Date), 5)
SET IDENTITY_INSERT [dbo].[staff_change] OFF
ALTER TABLE [dbo].[rate_change]  WITH CHECK ADD  CONSTRAINT [FK_Rate_Position] FOREIGN KEY([position_id])
REFERENCES [dbo].[position] ([id])
GO
ALTER TABLE [dbo].[rate_change] CHECK CONSTRAINT [FK_Rate_Position]
GO
ALTER TABLE [dbo].[staff_change]  WITH CHECK ADD  CONSTRAINT [FK_Staff_Department] FOREIGN KEY([department_id])
REFERENCES [dbo].[department] ([id])
GO
ALTER TABLE [dbo].[staff_change] CHECK CONSTRAINT [FK_Staff_Department]
GO
ALTER TABLE [dbo].[staff_change]  WITH CHECK ADD  CONSTRAINT [FK_Staff_Position] FOREIGN KEY([position_id])
REFERENCES [dbo].[position] ([id])
GO
ALTER TABLE [dbo].[staff_change] CHECK CONSTRAINT [FK_Staff_Position]
GO
USE [master]
GO
ALTER DATABASE [dbTariff] SET  READ_WRITE 
GO
