﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Tariff;

namespace Tariff
{
    public class MainController // вся бизнес логика проекта. при развитии проекта этот класс будет поделен на структуру классов. Пока проект небольшой - все в одном
    {        
        private MainViewModel mainViewModel; // корневой объект всех view model

        private MainDataProvider dal; // корневой объект всех data providers. только data providers имеют доступ к БД
        private Dictionary<Int32, String> positionDictionary; // словарь должностей - для конвертера positionId -> position.Name
        private Dictionary<Int32, String> departmentDictionary; // словарь отделов - для конвертера departmentId -> department.Name

        public MainController(MainControl mainControl) {
            try {
                dal = new MainDataProvider(); 

                AddTestData(); // если база пустая, добавит в справочники пару должностей и отделов: по ТЗ их не нужно добавлять через интерфейс

                // ****** создаем ViewModels для компонентов интерфейса

                IList<Position> positionList = dal.ListDataProvider.GetPositions(); // список должностей - для комбобоксов выбора должности
                positionDictionary = PositionToDictionary(positionList); // словарь должностей - для конвертера positionId -> position.Name

                IList<Department> departmentList = dal.ListDataProvider.GetDepartments(); // список отделов - для комбобоксов выбора должности
                departmentDictionary = DepartmentToDictionary(departmentList); // отделов должностей - для конвертера departmentId -> department.Name

                mainViewModel = new MainViewModel(departmentList, positionList); // корневой объект всех view model

                // *** заполняем ViewModel для контрола ввода изменений ставок
                mainViewModel.RateChangesViewModel.RateChangesListViewModel.RateChanges = dal.ListDataProvider.GetRateChanges(); // передаем список записей из БД
                mainViewModel.RateChangesViewModel.RateChangesListViewModel.PositionNameDictionary = positionDictionary; // передаем словарь должностей
                mainViewModel.RateChangesViewModel.AddRateChangeViewModel.AddRateChange += AddRateChangeViewModel_AddRateChange; // подписываемся на кнопку "добавить"
                // *** ^заполняем ViewModel для контрола ввода изменений ставок

                // *** заполняем ViewModel для контрола ввода изменений численности штата
                mainViewModel.StaffChangesViewModel.StaffChangesListViewModel.StaffChanges = dal.ListDataProvider.GetStaffChanges(); // передаем список записей из БД
                mainViewModel.StaffChangesViewModel.StaffChangesListViewModel.DepartmentNameDictionary = departmentDictionary; // передаем словарь должностей
                mainViewModel.StaffChangesViewModel.StaffChangesListViewModel.PositionNameDictionary = positionDictionary; // передаем словарь отделов
                mainViewModel.StaffChangesViewModel.AddStaffChangeViewModel.AddStaffChange += AddStaffChangeViewModel_AddStaffChange; // подписываемся на кнопку "добавить"
                // *** ^заполняем ViewModel для контрола ввода изменений численности штата

                // *** заполняем ViewModel для контрола отчета
                mainViewModel.ReportViewModel.DepartmentNameDictionary = departmentDictionary; // передаем словарь отделов для конвертера. в таблице из базы будут id. конвертером сконвертируем их в названия
                mainViewModel.ReportViewModel.RemakeReport += ReportViewModel_RemakeReport; // подписываемся на кнопку создания отчета
                // *** ^заполняем ViewModel для контрола отчета

                // ****** ^создаем ViewModels для компонентов интерфейса

                mainControl.DataContext = mainViewModel; // отправляем заполненную ViewModel в UI

            }
            catch (Exception x) { // главная ожидаемая ошибка - "нет соединения к БД". Ее текст выводим msgbox-ом и приложение "сворачиваем"
                AppLog.Log(x);

                MessageBoxResult result = MessageBox.Show(x.Message, "Тарифы", MessageBoxButton.OK,  MessageBoxImage.Exclamation );
                Application.Current.Shutdown();

            }
        }

        private void ReportViewModel_RemakeReport(MainReportParams mainReportParams) { // обработчик кнопки "создать отчет"
            try {

                mainViewModel.ReportViewModel.LastError = null; // очищаем предыдущее сообщения об ошибке

                Reporter reporter = new Reporter(dal); // бизнес-компонент отвечающий за генерацию отчета

                IList<CompactReportLine> compactReport; // сюда получим отчет
                String falseCause; // сюда получим сообщение об ошибке - например, "надостаточно данных" если отсутствует информация о размере ставки, например
                Boolean result = reporter.MakeReport(mainReportParams.LowerBound, mainReportParams.UpperBound, out compactReport, out falseCause); // создается отчет

                if (!result) {
                    mainViewModel.ReportViewModel.LastError = falseCause; // выводим в UI сообщение об ошибке: заполняем view model, его пропертя забайндена в поле UI
                    return;
                }
                
                mainViewModel.ReportViewModel.ReportData = compactReport; // отправляем отчет в UI. пропертя .ReportData забайдена в DataGrid
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void AddRateChangeViewModel_AddRateChange(RateChange newRateChange) { // обработчик кнопки "Добавить изменение ставки"
            try {
                mainViewModel.RateChangesViewModel.AddRateChangeIsEnabled = false; // делаем форму недоступной - во избежание двойных кликов

                Boolean alreadyExist = dal.AddDataProvider.RateChangeIsAlreadyExist(newRateChange.PositionId, newRateChange.Started); // если на этот месяц уже введено изменение, то дубликат не позволяется

                if (alreadyExist) { // в поле "сообщение об ошибке" выводим сообщене об ошибке
                    String falseCause = String.Format(new System.Globalization.CultureInfo("ru-RU"), "Изменение ставки на {0:MMMM} {0:yyyy} для должности {1} уже установлено.", newRateChange.Started, positionDictionary[newRateChange.PositionId]);
                    mainViewModel.RateChangesViewModel.AddRateChangeViewModel.LastError = falseCause;
                    return; 
                }


                dal.AddDataProvider.Add(newRateChange); // добавляем в БД обратившись к соответствующему data-провайдеру
                mainViewModel.RateChangesViewModel.RateChangesListViewModel.RateChanges.Add(newRateChange); // в ObservableCollection RateChanges модели добавляем строчку. UI ее подхватит
                ClearAddRateForm(); // очищаем форму ввода
                mainViewModel.RateChangesViewModel.AddRateChangeIsEnabled = true; // делаем форму ввода снова доступной
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void AddStaffChangeViewModel_AddStaffChange(StaffChange newStaffChange) { // обработчик кнопки "Добавить изменение штата"
            try {
                mainViewModel.StaffChangesViewModel.AddStaffChangeIsEnabled = false; // делаем форму недоступной - во избежание двойных кликов

                Boolean alreadyExist = dal.AddDataProvider.StaffChangeIsAlreadyExist(newStaffChange.DepartmentId, newStaffChange.PositionId, newStaffChange.Started); // проверяем если ли информация по этому отделу по этой должности на этот месяц уже в БД. Дубликаты не допускаются

                if (alreadyExist) { // в поле "сообщение об ошибке" выводим сообщене об ошибке
                    String falseCause = String.Format(new System.Globalization.CultureInfo("ru-RU"), "Изменение количества сотрудников на {0:MMMM} {0:yyyy} в отдел {1} для должности {2} уже установлено.", newStaffChange.Started, departmentDictionary[newStaffChange.DepartmentId], positionDictionary[newStaffChange.PositionId]);
                    mainViewModel.StaffChangesViewModel.AddStaffChangeViewModel.LastError = falseCause;
                    return;
                }


                dal.AddDataProvider.Add(newStaffChange); // добавляем в БД обратившись к соответствующему data-провайдеру
                mainViewModel.StaffChangesViewModel.StaffChangesListViewModel.StaffChanges.Add(newStaffChange); // в ObservableCollection StaffChanges модели добавляем строчку. UI ее подхватит
                ClearAddStaffForm(); // очищаем форму ввода
                mainViewModel.StaffChangesViewModel.AddStaffChangeIsEnabled = true; // делаем форму ввода снова доступной
            }
            catch (Exception x) {
                AppLog.Log(x);
            }
        }

        private void ClearAddRateForm() { // очистка формы ввода изменения ставок
            try {
                mainViewModel.RateChangesViewModel.AddRateChangeViewModel.Position = null;
                mainViewModel.RateChangesViewModel.AddRateChangeViewModel.Started = null;
                mainViewModel.RateChangesViewModel.AddRateChangeViewModel.Salary = null;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }

        }

        private void ClearAddStaffForm() { // очистка формы ввода изменения численности штата
            try {
                mainViewModel.StaffChangesViewModel.AddStaffChangeViewModel.Department = null;
                mainViewModel.StaffChangesViewModel.AddStaffChangeViewModel.Position = null;
                mainViewModel.StaffChangesViewModel.AddStaffChangeViewModel.Started = null;
                mainViewModel.StaffChangesViewModel.AddStaffChangeViewModel.Number = null;
            }
            catch (Exception x) {
                AppLog.Log(x);
            }

        }


        private Dictionary<Int32, String> PositionToDictionary(IList<Position> list) { // список должностей нужен в двух видах: как список и как словарь. чтобы не делать лишний запрос в БД, используем этот конвертер
            try {
                Dictionary<Int32, String> result = new Dictionary<Int32, String>();
                foreach (Position position in list) {
                    result.Add(position.ID, position.Name);
                }
                return result;
            }
            catch (Exception x) {
                AppLog.Log(x);
                return null;
            }
        }

        private Dictionary<Int32, String> DepartmentToDictionary(IList<Department> list) { // список отделов нужен в двух видах: как список и как словарь. чтобы не делать лишний запрос в БД, используем этот конвертер
            try {
                Dictionary<Int32, String> result = new Dictionary<Int32, String>();
                foreach (Department department in list) {
                    result.Add(department.ID, department.Name);
                }
                return result;
            }
            catch (Exception x) {
                AppLog.Log(x);
                return null;
            }
        }

        private void AddTestData() { // если база пустая, добавит в справочники пару должностей и отделов: по ТЗ их не нужно добавлять через интерфейс
            try {
                Int32 positionsCount = dal.ListDataProvider.GetPositionsCount();

                if (positionsCount == 0) {
                    dal.AddDataProvider.Add(new Position("Инженер конструктор"));
                    dal.AddDataProvider.Add(new Position("Инженер технолог"));

                    dal.AddDataProvider.Add(new Department("КБ"));
                    dal.AddDataProvider.Add(new Department("Технологический отдел"));
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
            }

        }
    }
}
