﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    // класс - справочник. по отделу и должности вернет количество людей в штате по конкретному месяцу
    public class PositionHelper { 
        DateValuer<Int32> dateValuer; // используется универсальный "быстрый, с курсором" класс поиска значения соответствующего дате по списку "новое значение, дата введения"
        Boolean noStaffInformation; // есть ли в данных класса хоть какая-то информация

        public PositionHelper(MainDataProvider dal, Int32 departmentId, Int32 positionId, DateTime lowerDate, DateTime upperDate) {
            try {
                dateValuer = new DateValuer<Int32>(); // рабочая лошадка - класс поиска значения соответствующего дате по списку "новое значение, дата введения"

                // из data-провайдера получаем запись которая хранит данные о числе людей в штате на дату начала запрашиваемого период
                // Дата этой записи = дате нижней границе периода, либо если такой нет - это предыдущая перед ней запись
                StaffChange actualLowerDateRateChange = dal.StaffChangesDataProvider.PositionHelper_GetActualLowerDateChange(departmentId, positionId, lowerDate); 

                if (actualLowerDateRateChange == null) { // нет информации по должности на интересующую нас начальную дату. не страшно. может об этом и запроса не будет.
                    // тогда вычисляем просто минимальную дату по данной должности в отделе
                    actualLowerDateRateChange = dal.StaffChangesDataProvider.PositionHelper_GetLowerDateChange(departmentId, positionId, upperDate);
                    if (actualLowerDateRateChange == null) { // совсем нет информации по ставке на эту должность
                        // ну и ладно, получается на ней никто не работал
                        noStaffInformation = true; // даже "рабочую лошадку" dateValuer не создаем. нам нечего ей скормить
                        return;
                    }
                }

                noStaffInformation = false; // какая-то минимальная дата изменений по штату должности в отделе есть. с ней будем работать

                DateTime actualLowerDate = actualLowerDateRateChange.Started; // дата - нижний порог интересующих нас записей в БД

                // составляем упорядоченный список изменений по должности
                IList<StaffChange> orderedStaffChanges = dal.StaffChangesDataProvider.PositionHelper_GetOrderedStaffChanges(departmentId, positionId, actualLowerDate, upperDate);

                dateValuer = new DateValuer<Int32>(); // класс который по списку "новое значение, дата введения" будет возвращать value по заданной дате
                foreach (StaffChange staffChange in orderedStaffChanges) { // каждую запись по отделу
                    dateValuer.Add(staffChange.Started, staffChange.Number); // вносим в справочник "рабочей лошадки"
                }

            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        // для отдела и должности по заданной дате вернет размер штата
        public Int32 GetStaffForDate(DateTime date) { // этот метод будет вызываться с ранних дат к поздним - это исключит постоянные пробежки по поиску нужной даты

            if (noStaffInformation) { // вообще ни одной записи в БД об изменении размера штата в этом отделе по данной должности
                return 0; // возвращаем "работало ноль работников"
            }

            Int32 staff;
            Boolean result = dateValuer.TryGetValueForDate(date, out staff);

            if (result) {
                return staff;
            }
            else {
                return 0; // нет данных на эту дату, значит не было работников по данной специальности в этом отделе
            }
            
        }
    }
}
