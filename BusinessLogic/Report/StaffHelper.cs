﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    public class StaffHelper { // по департаменту, специальности и дате возвращает число работников
        private Dictionary<Int32, DepartmentHelper> departmentHelpers;
        private IEnumerable<Int32> departmentIds;

        public IEnumerable<Int32> DepartmentIds { get { return departmentIds; } }

        public StaffHelper(MainDataProvider dal, RateHelper rateHelper, DateTime lowerDate, DateTime upperDate) {
            try {
                departmentHelpers = new Dictionary<Int32, DepartmentHelper>();

                // составляем список интересующих нас департаментов
                departmentIds = dal.StaffChangesDataProvider.Reporter_PotentialDepartments(upperDate);

                foreach (Int32 departmentId in departmentIds) {
                    DepartmentHelper departmentHelper = new DepartmentHelper(rateHelper, dal, departmentId, lowerDate, upperDate);
                    departmentHelpers.Add(departmentId, departmentHelper);
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }




        public Boolean GenerateLongReport(DateTime lowerDate, DateTime upperDate, out IList<DepartmentDateSalary> report, out String falseCause) { 
            // может вернуть null - значит недостаточно информации для создания отчета
            try {
                IList<DepartmentDateSalary> resultList = new List<DepartmentDateSalary>();

                DateTime dateIndex = lowerDate;  
                do { // бежим по всему интересующему нас интервалу прибавляя по месяцу
                    foreach (Int32 departmentId in departmentIds) {
                        Decimal departmentSalary;
                        Boolean result = departmentHelpers[departmentId].GetTotalForDate(dateIndex, out departmentSalary, out falseCause);
                        if (!result) {
                            report = null;
                            return false;
                        }
                        resultList.Add(new DepartmentDateSalary(departmentId, dateIndex, departmentSalary));
                    }

                    
                    dateIndex = PlusOneMonth(dateIndex);
                    
                } while (dateIndex <= upperDate);

                falseCause = null;
                report = resultList;
                return true;
            }
            catch (Exception x) {
                AppLog.Log(x);
                report = null;
                falseCause = null;
                return false;
            }
        }


        private DateTime PlusOneMonth(DateTime date) { // прибавляет 1 месяц к дате. при "сворачивании" отчета дата окончания периода будет составлена в терминах ТЗ (например, "до 01.12") когда в терминах метода "дата месяца соответствующего записи", а не "когда этот месяц заканчивается"
            if (date.Month < 12) {
                return new DateTime(date.Year, date.Month + 1, 1);
            }
            else {
                return new DateTime(date.Year + 1, 1, 1);
            }
        }

    }


}
