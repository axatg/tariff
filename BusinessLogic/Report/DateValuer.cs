﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    public class DateValuer<T> { // класс который для списка "дата - новое значение" будет возвращать значение по запрашиваемой дате. 
        // для избежания постоянных пробежек по списку есть правило: запрашивающий должен спрашивать даты начиная с ранних к более поздним

        private IList<DateValuePair<T>> list; // хранилище данных
        private Int32 index; // курсор указывающий "до какой даты дошли" - указывает на индекс в "хранилище"
        private DateTime lowerDate; // минимальная дата по которой в хранилище есть информация
        private Boolean lowerDateIsSet; // флажок, указывающий что минимальная дата уже сохранена        

        public DateValuer() {
            list = new List<DateValuePair<T>>(); // создаем хранилище
            lowerDateIsSet = false; // выставляем флажок о том что минимальная дата еще не сохранена
            index = 0; // выставляем курсор на первую запись в списке. если запрос придет по дате, меньшей чем в первой записи, мы его отклоним проверкой (date < lowerDate)
        }

        // метод для заполнения справочника об изменениях. даты обязательно поступают от более ранних к более поздним
        public void Add(DateTime date, T value) { // упорядоченно от старых к новым добавляются даты и значения
            list.Add(new DateValuePair<T>() { Date = date, Value = value });

            if (!lowerDateIsSet) { // первая запись хранит минимальное значение даты. из нее запоминаем
                lowerDate = date;
                lowerDateIsSet = true;
            }        

        }

        // основной метод класса. по дате возвращает value вычисленной по списку-справочнику
        public Boolean TryGetValueForDate(DateTime date, out T value) { // этот метод будет вызываться с ранних дат к поздним, причем по каждому месяцу подряд - это исключит постоянные пробежки по поиску нужной даты
            try {

                if (date < lowerDate) { // запрашивают информацию по более ранней дате чем у нас есть в справочнике. таких value у нас нет
                    value = default(T); // возвращаем пустое value: что-то надо возвращать
                    return false; // возвращаем false: try оказался безуспешным
                }        

                // *** если надо, двигаем наш курсор
                // достаточно двинуть на соседнюю запись т.к. запросы у нас идут по каждому месяцу подряд
                if (index < list.Count - 1) { // если мы не на последнем элементе
                    if (date >= list[index + 1].Date) index++;  // если мы добежали до следущей даты, то прыгаем на нее
                }
                // *** ^если надо, двигаем наш курсор

                value = list[index].Value;
                return true;
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }

        }
    }

    public struct DateValuePair<T> { // структура данных класса
        public DateTime Date; // дата начала действия value
        public T Value; // само value
    }
}
