﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    // класс - справочник. назначение: по id отдела и дате вернуть ФОТ отдела в этом месяце
    public class DepartmentHelper { 
        private Dictionary<Int32, PositionHelper> positionHelpers; // подклассы которые дадут информацию по отдельным должностям в отделе
        private IEnumerable<Int32> positionIds; // список должностей в отделе
        private RateHelper rateHelper; // справочник который по паре "должность-месяц" вернет размер оплаты труда по должности в данном месяце
        private Int32 departmentId; // id отдела для которого создан экземпляр класса
        private MainDataProvider dal; // центральный компонент доступа к БД

        public DepartmentHelper(RateHelper rateHelper, MainDataProvider dal, Int32 departmentId, DateTime lowerDate, DateTime upperDate) {
            try {
                this.rateHelper = rateHelper; // справочник который по паре "должность-месяц" вернет размер оплаты труда по должности в данном месяце
                this.departmentId = departmentId; // id отдела для которого создан экземпляр класса
                this.dal = dal; // центральный компонент доступа к БД

                positionHelpers = new Dictionary<Int32, PositionHelper>(); // подклассы которые дадут информацию по отдельным должностям в отделе
                
                positionIds = dal.StaffChangesDataProvider.DepartmentHelper_GetDepartmentPositions(departmentId, upperDate); // составляем список должностей в отделе

                foreach (Int32 positionId in positionIds) { // для каждой должности создаем справочник который по "отдел, должность, месяц" выдаст число сотрудников
                    PositionHelper positionHelper = new PositionHelper(dal, departmentId, positionId, lowerDate, upperDate);
                    positionHelpers.Add(positionId, positionHelper);
                }

            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }

        }

        // основной метод класса. по запрашиваемой дате выдаст ФОТ по отделу за запрашиваемый месяц
        public Boolean GetTotalForDate(DateTime date, out Decimal departmentSalary, out String falseCause) { // этот метод будет вызываться с ранних дат к поздним - это исключит постоянные пробежки по поиску нужной даты
            // false - Значит нет достаточно информации в rate_changes
            try {
                departmentSalary = 0; // начинаем считать сумму ФОТ по отделу

                foreach (Int32 positionId in positionIds) { // по каждой должности в отделе
                    Int32 positionStaff = positionHelpers[positionId].GetStaffForDate(date); // получаем численность работников по должности

                    if (positionStaff > 0) { // если были люди в отделе на этой должности. их могло не быть т.к. число работников может быть равным нулю
                        Decimal positionSalary;
                        Boolean thereIsSalaryInfo = rateHelper.TryGetSalaryForDate(positionId, date, out positionSalary, out falseCause ); // запрашиваем ФОТ по данной должности в данном отделе за данный месяц

                        if (!thereIsSalaryInfo) { // если данных нет. например, нет информации об размере оплаты в таблице размеров ставки
                            departmentSalary = 0;
                            return false; // возвращаем false: нет возможности вычислить запрашиваемые данные. falseCause - причина "невозможности" передается вверх из вызываемого метода
                        }

                        departmentSalary += positionStaff * positionSalary; // плюсуем ФОТ по должности в ФОТ отдела
                    }
                }

                falseCause = null; // зачищаем причину "невозможности" вычисления результата
                return true; // результат получен успешно
            }
            catch (Exception x) {
                AppLog.Log(x);
                departmentSalary = 0;                
                falseCause = String.Format(new System.Globalization.CultureInfo("ru-RU"), "Ошибка при получении суммы по отделу {0} на {1:MMMM} {1:yyyy}", dal.ListDataProvider.DepartmentNameById(departmentId), date);
                return false; // в falseCause пишем подробности - на чем остановилась подготовка данных для отчета
            }
        }
    }
}