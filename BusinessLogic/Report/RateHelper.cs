﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    // по любой должности по интересующему месяцу этот класс возвращает размер зарплаты
    // внутри юзает подобъекты отвечающий каждый за свою должность
    public class RateHelper { // хранит коллекцию хэлперов по каждой должности и может по паре должность-дата сообщить размер ставки

        private Dictionary<Int32, PositionRateHelper> positionHelpers; // словарь объектов - по одному на каждую должность
        private MainDataProvider dal; // data-провайдер. весь доступ к БД - только через него

        // чтоб лишнее не читать из БД: задаются на вход рамки: список позиций и диапазон интесующих дат
        public RateHelper(MainDataProvider dal, IList<Int32> positionIds, DateTime lowerDate, DateTime upperDate) {             
            try {
                this.dal = dal; // он может разово понадобиться в составлении ответа - в случае ошибки надо будет сконвертить id должности в ее название

                positionHelpers = new Dictionary<Int32, PositionRateHelper>(); // словарь исполнителей - по конкретным должностям

                foreach (Int32 positionId in positionIds) { // для каждой интересующей нас должности
                    PositionRateHelper positionRateHelper = new PositionRateHelper(dal, positionId); // создаем испольнителя по этой должности
                    positionRateHelper.Prepare(lowerDate, upperDate); // заполняем его данными

                    positionHelpers.Add(positionId, positionRateHelper); // добавляем в словарь исполнителей

                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }

        // основной справочный метод
        // на вход: позиция и дата
        // вы выход: размер оплаты по позиции в запрашиваемом месяце
        // в БД может не быть данных об оплате. тогда метод вернет false, а в falseCause будет информация каких именно данных в БД не хватает
        public Boolean TryGetSalaryForDate(Int32 positionId, DateTime date, out Decimal salary, out String falseCause) {
            try {
                if (!positionHelpers.ContainsKey(positionId)) { // есть ли исполнитель по запрашиваемой должности?
                    salary = 0; // out-параметр надо заполнять
                    falseCause = String.Format(new System.Globalization.CultureInfo("ru-RU"), "Нет информации о размере оплаты должности {0} на {1:MMMM} {1:yyyy}", dal.ListDataProvider.PositionNameById(positionId), date);
                    return false;
                }

                PositionRateHelper positionRateHelper = positionHelpers[positionId]; // это исполнитель по конкретной должности

                
                Boolean result = positionRateHelper.TryGetSalaryForDate(date, out salary); // он квалифицированно ответит на запрос

                if (result) { // есть интересующая нас информация
                    falseCause = null; // очищаем falseCause
                    return true; 
                }
                else { // нет информации об оплате на заданную дату
                    // заполняем подробности: какая именно должность, какой месяц. Эта информация будет выведена пользователю
                    falseCause = String.Format(new System.Globalization.CultureInfo("ru-RU"), "Нет информации о размере оплаты должности {0} на {1:MMMM} {1:yyyy}", dal.ListDataProvider.PositionNameById(positionId), date);
                    return false; // не смогли дать ответ на запрос о размере оплаты
                }
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }
    }
}
