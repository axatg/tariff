﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {
    // один экземпляр класс создается на одну должность. по запрашиваемому месяцу класс возвращает размер оплаты по должности в этом месяце
    public class PositionRateHelper { // один класс - на одну должность. сообщает какова была ставка на запрашиваемую дату
        
        private Int32 positionId; // id должности
        private DateValuer<Decimal> rateHelper; // исполнитель - универсальный "быстрый, с курсором" класс поиска значения соответствующего дате по списку "новое значение, дата введения"
        private MainDataProvider dal; // провайдер данных - класс доступа к БД

        public PositionRateHelper(MainDataProvider dal, Int32 positionId) {            
            this.positionId = positionId;
            this.dal = dal;
        }

        public void Prepare(DateTime lowerDate, DateTime upperDate) { // результат - достаточно данных в таблице или нет
            try {

                // из data-провайдера получаем запись которая хранит данные о размере оплаты на дату начала запрашиваемого период
                // Дата этой записи = дате нижней границе периода, либо если такой нет - это предыдущая перед ней запись
                RateChange actualLowerDateRateChange = dal.RateChangesDataProvider.PositionRateHelper_GetActualLowerDateChange(positionId, lowerDate);

                if (actualLowerDateRateChange == null) { // нет информации по должности на интересующую нас начальную дату. не страшно. может, на должности на эту дату никто и не работал
                    // тогда берем просто самую минимальную дату в БД
                    actualLowerDateRateChange = dal.RateChangesDataProvider.PositionRateHelper_GetLowerDateChange(positionId, upperDate);
                    if (actualLowerDateRateChange == null) { // совсем нет информации по ставке на эту должность
                        // rateHelper остается null - это мы будем проверять и возвращать "нет информации" если запрос к классу все же поступит
                        return;
                    }
                }

                DateTime actualLowerDate = actualLowerDateRateChange.Started; // нижний порог интересующих нас записей в БД

                // составляем упорядоченный список изменений по должности
                IList<RateChange> orderedRateChanges = dal.RateChangesDataProvider.PositionRateHelper_GetOrderedRateChanges(positionId, actualLowerDate, upperDate);

                rateHelper = new DateValuer<Decimal>(); // рабочая лошадка будет выполнять всю алгоритмическую работу
                foreach (RateChange rateChange in orderedRateChanges) { // каждую запись об изменениях размера оплаты по данной должности за интересующий нас период
                    rateHelper.Add(rateChange.Started, rateChange.Salary);  // вносим в справочник лошадки
                }

            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }
        }
        
        public Boolean TryGetSalaryForDate(DateTime date, out Decimal salary) { // этот метод будет вызываться с ранних дат к поздник - это исключит постоянные пробежки по поиску нужной даты
            if (rateHelper == null) { // никаких данных нет: когда готовили данные за интересующий период оказалось что ни одной записи в БД для класса не оказалось
                salary = 0; // out-параметр надо заполнить
                return false; // false говорит что сумма оплаты на поступивший запрос вычислена быть не может
            }

            return rateHelper.TryGetValueForDate(date, out salary); // даем "рабочей лошадке" квалифицированно ответить на поступивший запрос
        }
    }
}
