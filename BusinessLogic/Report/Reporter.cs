﻿using CommonClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tariff {

    // бизнес-объект задача которого - сформировать отчет для контрола "Report"
    public class Reporter {
        private MainDataProvider dal; // провайдер данных. весь доступ к БД через него

        public Reporter(MainDataProvider dal) {
            this.dal = dal;
        }

        // метод вернет сам составленный отчет и true если отчет составлен
        // возможна ситуация когда данных в БД недостаточно для составления отчета. например, отсутствует информация о размере оплаты по должности. 
        // тогда отчет составлен не будет: метод вернет false и причину по которой не составлен отчет - строчку в falseCause
        public Boolean MakeReport(DateTime lowerDate, DateTime upperDate,out IList<CompactReportLine> compactReport, out String falseCause) {
            try {
                upperDate = MinusOneMonth(upperDate); // ТЗ требует запрашивать дату окончания периода по номеру месяца. например: до 01.11.18. метод работает с "интересующими месяцами"
                // т.е. "нужен отчет до 01.11.18" в терминах метода значит "нужен отчет до октября 2018 включительно"
                // поэтому вычитаем месяц

                // *** чтобы не тянуть из БД все данные которых может быть очень много, зная диапазон отчета ограничиваем список интересующих должностей и отделов

                // список интересующих нас должностей: те что стартовали до upperDate включительно (стартовали с составом в 0 человек не считаются)
                IList<Int32> potentialPositions = dal.StaffChangesDataProvider.Reporter_PotentialPositions(upperDate);

                // составляем список интересующих нас департаментов: те что стартовали до upperDate включительно (стартовали с составом в 0 человек не считаются)
                IList<Int32> potentialDepartments = dal.StaffChangesDataProvider.Reporter_PotentialDepartments(upperDate);

                // *** ^чтобы не тянуть из БД все данные которых может быть очень много, зная диапазон отчета ограничиваем список интересующих должностей и отделов

                RateHelper rateHelper; // вспомогательный объект который по id должности и дате возвращает размер оплаты. не лазая в БД при каждом обращении
                rateHelper = new RateHelper(dal, potentialPositions, lowerDate, upperDate);
                

                StaffHelper staffHelper = new StaffHelper(dal, rateHelper, lowerDate, upperDate); // вспомогательный объект который по отделу, должности, дате вернет соответствующее число сотрудников
                IList<DepartmentDateSalary> longReport;                

                // "промежуточный" отчет. тот же отчет который должен получиться в итоге, но не "свернутый" по отделам и суммам ФОТ
                // строки отчета: месяц - отдел - ФОТ. потом мы его "свернем"
                Boolean result = staffHelper.GenerateLongReport(lowerDate, upperDate, out longReport , out falseCause); // создание "промежуточного" отчета

                if (!result) { // если не был составлен отчет - например, не хватило данных
                    compactReport = null;
                    return false; // сообщаем что отчет составлен не был. а причина "несоставления" - подымается вверх в переменной falseCause
                }
                
                compactReport = GenerateCompactReport(longReport); // "сворачиваем" отчет: группируем по отделам и суммам ФОТ

                falseCause = null; // очищаем причину несоставления отчета
                return true; // отчет составлен успешно
            }
            catch (Exception x)  {
                AppLog.Log(x);
                compactReport = null;
                falseCause = null;
                return false;
            }
        }

        public IList<CompactReportLine> GenerateCompactReport(IList<DepartmentDateSalary> longReport) { // группирует отчет " месяц - отдел - сумма ФОТ" по отделу и сумме ФОТ
            try {

                IList<CompactReportLine> result = new List<CompactReportLine>(); // здесь будет записан результат

                IEnumerable<IGrouping<Int32, DepartmentDateSalary>> groupedByDepartment = longReport.GroupBy(l => l.DepartmentId); // линком группируем по отделам

                foreach (var department in groupedByDepartment) { // для каждого отдела
                    Int32 departmentId = department.Key; // id отдела

                    DateTime? intervalLower = null; // нижняя граница формируемой строчки отчета
                    Decimal intervalSalary = 0; // сумма ФОТ формируемой строчки отчета
                    DateTime? intervalUpper = null; // верхняя граница формируемой строчки отчета

                    foreach (var monthLine in department) { // для каждого месяца по данному отделу
                        if (intervalLower == null) { // если надо формировать новую строчку отчета
                            intervalLower = monthLine.Date;
                            intervalSalary = monthLine.Salary;
                            intervalUpper = monthLine.Date;  // верхняя граница строчки - временная. она будет перезаписана если следующий месяц с той же ФОТ. либо не будет перезаписана если уже в следующем месяце сумма ФОТ изменится
                        }
                        else { // есть "формируемся строчка отчета"

                            if (monthLine.Salary == intervalSalary) { // в этом месяце сумма ФОТ совпадает с суммой формируемой строчки
                                intervalUpper = monthLine.Date; // просто обновляем дату верхнего интервала формируемой строчки
                            }
                            else { // в этом месяце сумма ФОТ изменилась по сравнению с уже формируемой строчкой
                                result.Add(new CompactReportLine(departmentId, (DateTime)intervalLower, PlusOneMonth((DateTime)intervalUpper), intervalSalary)); // строка завершена - отправляем ее в result

                                // создаем новую "формируемую строку отчета"
                                intervalLower = monthLine.Date; 
                                intervalSalary = monthLine.Salary;
                                intervalUpper = monthLine.Date;

                            }
                        }

                    } // for monthLine

                    // по всем месяцам пробежались, осталась незакрытая "формируемая строка отчета"
                    if (intervalLower != null) { // null - если вообще ни одной строки не было сформировано. игнорируем этот случай
                        result.Add(new CompactReportLine(departmentId, (DateTime)intervalLower, PlusOneMonth((DateTime)intervalUpper), intervalSalary)); // дописываем "формируемую" строку в result
                    }

                }
                return result;
            }
            catch (Exception x) {
                AppLog.Log(x);
                throw x;
            }

        }

        private DateTime MinusOneMonth(DateTime date) { // вычитает 1 месяц из даты. нужен для конвертации даты окончания периода отчета из терминов ТЗ в термины метода
            if (date.Month > 1) {
                return new DateTime(date.Year, date.Month - 1, 1);
            }
            else {
                return new DateTime(date.Year - 1, 12, 1);
            }
        }

        private DateTime PlusOneMonth(DateTime date) { // прибавляет 1 месяц к дате. при "сворачивании" отчета дата окончания периода будет составлена в терминах ТЗ (например, "до 01.12") когда в терминах метода "дата месяца соответствующего записи", а не "когда этот месяц заканчивается"
            if (date.Month < 12) {
                return new DateTime(date.Year, date.Month + 1, 1);
            }
            else {
                return new DateTime(date.Year + 1, 1, 1);
            }
        }
    }


}
